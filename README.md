Smart Home Web Application
==========================
The interface for the Smart Home Project for Maxtrack - Final Year Project 2014 (Swinburne)

## TODO: ##

- [ ] Socket Driver
    - [x] Receive and Send Commands to Python Server
    - [ ] Get Automation Working

- [x] Login Page
    - [x] Login from Remote IP Address with Password
    - [x] Login from Local IP Address with Pin
    - [x] Uncache login page

- [x] Rooms Page
    - [x] Query from database
    - [x] Edit Room name (AJAX)
    - [x] Cache Page

- [x] Devices Page
    - [x] Query from database
    - [x] Edit Devices Name (AJAX)
    - [x] Cache Page

- [x] Presets Page
    - [x] Query from database
    - [x] New Preset
    - [x] Edit Preset
    - [x] Remove Preset (AJAX)
    - [x] Validate New Preset form
    - [x] Validate Edit Preset form
    - [x] Cache Presets Page
    - [x] uncache Edit and New pages

- [x] Automation Page
    - [x] Query from database
    - [x] New Automation
    - [x] Edit Automation
    - [x] Remove Automation (AJAX)
    - [x] Validate New Automation form
    - [x] Validate Edit Automation form
    - [x] Cache Automation Page
    - [x] uncache Edit and New pages

- [x] Schedule Task Page
    - [x] Query from database
    - [x] New Schedule Task Page
    - [x] Edit Scheduling Task
    - [x] Remove Scheduling Task (AJAX)
    - [x] Validate New Schedule Task form
    - [x] Validate Edit Schedule Task form
    - [x] Cache Scheduling Task Page
    - [x] uncache Edit and New pages

- [x] Settings Page
    - [x] Allow master user change PIN
    - [x] Allow master user change Password
    - [x] Allow normal user change PIN
    - [x] Allow normal user change Password
    - [x] Allow master user change email
    - [x] Allow normal user change email

- [x] CLI Controller
    - [x] Delete logs
    - [x] Delete caches
    - [x] Make cron job call CLI Controller

- [x] Cron controller
    - [x] Convert Scheduled Task to cron jobs
    - [x] Make enabling/disabling cron jobs easy

- [x] Manage different type of user's view!
    - [x] use JS to change user's view
