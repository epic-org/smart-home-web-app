<!DOCTYPE html>
<html>
	<head>
    <?=$head?>
	</head>
	<body>
		<div class="container">
			<?=$topstrip?>
			<div class="page-header">
                <h1><span id="room-name"></span><br /><small>New Preset</small></h1>
			</div> <!-- /.page-header -->
			<?=form_open(current_url(), array('role' => 'form'))?>
            <?=format_error(validation_errors())?>
            <div class="row row-form-name"><input type="text" value="" name="preset_name" class="form-control input-lg" id="preset-name" placeholder="<?=set_value('preset_name', 'Enter new preset name')?>" autofocus></div>
			<div class="row">
				<div class="panel-group" id="accordion">
                    <?=$presets['new_preset'][LIGHT]?>
                    <?=$presets['new_preset'][MOTOR]?>
                    <?=$presets['new_preset'][AC]?>
				</div> <!-- /panel-group -->
			</div> <!-- /row -->
			<?=$presets['alerts']['green_panels']?>
			<div class="row row-submit">
				<button type="submit" class="btn btn-primary btn-lg" name="submit" data-loading-text="Saving..."><span class="glyphicon glyphicon-save"></span>&nbsp;Save</button>
				<a href="<?=site_url($href['presets'] . '/' . $this->session->userdata('room_id'))?>" class="btn btn-default btn-lg">Cancel</a>
			</div> <!-- /row-submit -->
            <?=form_close()?>
		</div> <!-- /container -->
        <?=$hidden?>
		<?=$settings?>
        <?=$js?>
	</body>
</html>
