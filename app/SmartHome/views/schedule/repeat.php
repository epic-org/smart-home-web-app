<div class="panel panel-default">
	<div class="checkbox schedule-checkbox">
		<label><input type="checkbox" id="repeat">Repeat</label>
	</div>						
	<div class="btn-group">
        <?php /** NOTE: The order of days are important because of how the js check for the values.
        Changing the order of days will break the code. It should be arranged by highest value first in descending order. **/ ?>
		<button class="btn btn-default btn-week" type="button" disabled="disabled" value="64">Sunday</button>
		<button class="btn btn-default btn-week" type="button" disabled="disabled" value="32">Monday</button>
		<button class="btn btn-default btn-week" type="button" disabled="disabled" value="16">Tuesday</button>
		<button class="btn btn-default btn-week" type="button" disabled="disabled" value="8">Wednesday</button>
		<button class="btn btn-default btn-week" type="button" disabled="disabled" value="4">Thursday</button>
		<button class="btn btn-default btn-week" type="button" disabled="disabled" value="2">Friday</button>
		<button class="btn btn-default btn-week" type="button" disabled="disabled" value="1">Saturday</button>
        <input type="hidden" name="schedule[day]" id="week" value="<?= isset($schedule_state) ? $schedule_state->day : '0' ?>">
	</div>
</div>
