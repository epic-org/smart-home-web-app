<div class="form-group">
	<label for="time">Time</label>
    <input name="schedule[time]" value="<?= isset($schedule_state) ? format_from($schedule_state->time, 'H:i:s', 'h:i A') : '' ?>" type="text" class="form-control schedule-readonly" id="time" placeholder="12:00 AM" readonly>
</div>
<div class="form-group">
	<label for="date">Date</label>
    <input name="schedule[date]" value="<?= isset($schedule_state) ? ( ! empty($schedule_state->date) ?  format_from($schedule_state->date, 'Y-m-d', 'd/m/Y') : '') : '' ?>" type="text" class="form-control schedule-readonly" id="date" data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYY" readonly>
</div>
