<label for="schedule-preset">Select preset</label>
<select name="preset_id" id="schedule-preset" class="form-control schedule-select">
    <!-- TODO: Tell user to add new preset first if empty? -->
    <?php foreach ($presets as $preset): ?>
    <option value="<?=$preset->id?>" <?= isset($schedule_state) ? $preset->id == $schedule_state->preset_id ? 'selected' : '' : '' ?>><?=$preset->name?></option>
    <?php endforeach ?>
</select>
