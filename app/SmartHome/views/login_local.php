<!DOCTYPE html>
<html>
	<head>
    <?=$head?>
	</head>
	<body>
        <?php // TODO Beautify the errors with css. Class can be edited at line 8 ?>
        <?php $err = validation_errors('<div class="error text-center">', '</div>'); ?>
		<div class="container <?=($this->session->flashdata('error') OR strlen($err) > 0) ? 'shake-class' : ''?>">
			<div class="row">
            <?=form_open(current_url(), array('id' => 'login', 'role' => 'form'))?>
				<div class=" center-block">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<h1 class="text-center">PIN</h1>
                            <?=$err?>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 text-center">
                            <div class="form-group has-feedback <?=($this->session->flashdata('error') OR strlen($err) > 0) ? 'has-error' : ''?>">
                                <input type="password" name="pin" class="input-lg form-control" id="pin" readonly="readonly" maxlength="4">
                                    <span class="glyphicon glyphicon-remove form-control-feedback <?=($this->session->flashdata('error') OR strlen($err) > 0) ? '' : 'hidden'?>" aria-hidden="true" id="error-glyph" ></span>
                            <span id="wrongPin" class="sr-only">(error)</span>
                            </div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<a href="#" class="btn btn-default btn-xlg">1</a>
							<a href="#" class="btn btn-default btn-xlg">2</a>
							<a href="#" class="btn btn-default btn-xlg">3</a>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<a href="#" class="btn btn-default btn-xlg">4</a>
							<a href="#" class="btn btn-default btn-xlg">5</a>
							<a href="#" class="btn btn-default btn-xlg">6</a>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<a href="#" class="btn btn-default btn-xlg">7</a>
							<a href="#" class="btn btn-default btn-xlg">8</a>
							<a href="#" class="btn btn-default btn-xlg">9</a>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<a href="#" class="btn btn-default btn-xlg">*</a>
							<a href="#" class="btn btn-default btn-xlg">0</a>
							<a href="#" class="btn btn-default btn-xlg">#</a>
						</div>
					</div>
					<!-- TODO: Verify that it looks ok -->
			        <!-- <h4 id="alert-enable" class="alert alert-danger" role="alert"><strong>Attention!</strong> Please enable Cookies and Javascript! &nbsp;<span id="login-info" type="button" class="glyphicon glyphicon-question-sign" data-toggle="popover" title="<h4 class='login-popover-title'>What's this? <button type='button' class='close' onclick='closeLoginPopover()'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button></h4>" data-content="<p>Cookies/Javascript are currently disabled on your browser. To proceed, you must enable Cookies &amp; Javascript.</p> <u>More info (<strong>external links</strong>):</u><ul><li><a href='http://www.aboutcookies.org/default.aspx?page=5' target='_blank'>Cookies</a><small>&nbsp;(opens in new tab)</small></li><li><a href='http://www.enable-javascript.com/' target='_blank'>Javascript</a><small>&nbsp;(opens in new tab)</small></li></ul><strong>Still need help?</strong> Give us a call."></span></h4> -->
			        <?=$alerts['login']?>
					<div class="row">
						<div class="col-xs-12 col-md-12"><a href="#" id="clear" class="btn btn-danger btn-lg btn-block"><span class="glyphicon glyphicon-remove-sign pull-left"></span>Clear&nbsp;&nbsp;&nbsp;</a></div>
					</div>
				</div>
			</div> <!-- /row -->
            <div class="row select-auto"><button type="submit" name="guest" value="guest" id="guest" class="btn btn-primary btn-lg btn-lg center-block" disabled="disabled"><span class="glyphicon glyphicon-log-in pull-left"></span>&nbsp;Login as Guest</button></div>
			<hr />
			<div class="row"><button type="button" class="btn btn-warning btn-lg center-block" data-toggle="modal" data-target="#recoveryModal"><span class="glyphicon glyphicon-lock pull-left"></span>&nbsp;Forgot PIN?</button></div>
        </div> <!-- /container -->
        <?=form_close()?>
		<!-- Begin Modal -->
		<div class="modal fade" id="recoveryModal" tabindex="-1" role="dialog" aria-labelledby="recoveryModalLabel" aria-hidden="true">
		    <?=form_open(current_url(), array('role' => 'form'))?>
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		                <h4 class="modal-title" id="recoveryModalLabel">Forgot PIN</h4>
		            </div>
		            <div class="modal-body">
		             	<div class="form-group">
		             	    <label for="email">Recovery Email</label>
		             	    <input type="email" class="form-control" id="email" name="email" placeholder="Enter email that is registered with Smart Home">
		             	</div>
		             	<div class="alert alert-info" role="alert"><strong>Heads up!</strong> Internet connection is required at home for this feature to work.</div>
		            </div>
		            <div class="modal-footer">
		                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                <button type="submit" class="btn btn-primary" name="forgot_pin" value="submit">Submit</button>
		            </div>
		        </div> <!-- /.modal-content -->
		    </div> <!-- /.modal-dialog -->
		    <?=form_close()?>
		</div> <!-- /#recoveryModal -->
		<!-- End Modal -->
        <?=$js?>
	</body>
</html>
