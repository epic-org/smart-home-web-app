<!DOCTYPE html>
<html>
	<head>
    <?=$head?>
	</head>
	<body>
		<div class="container">
			<?=$topstrip?>
            <?=$room_name?>
			<?=$maintabs?>
			<div class="row">
				<div class="panel-group" id="accordion">
                    <?=$controls[LIGHT]?>
                    <?=$controls[MOTOR]?>
                    <?=$controls[AC]?>
				</div> <!-- /#accordion -->
			</div> <!-- /row -->
		</div> <!-- /container -->
        <?=form_hidden('rename', $ajax['rename_actuator'])?>
        <?=form_hidden('send_command', $ajax['send_command'])?>
        <?=$hidden?>
		<?=$settings?>
        <?=$js?>
	</body>
</html>
