<!-- For controls.php, presets.php, auto.php &amp; schedule.php views -->
<div class="row">
    <div class="col-md-12 room-title"><a href="<?=site_url()?>"><span class="btn btn-primary glyphicon glyphicon-th-list pull-left room-button"></span>
        <h1 id="room-name"><?=isset($room_name) ? $room_name : ''?></h1></a>
    </div>
</div> <!-- /row -->
