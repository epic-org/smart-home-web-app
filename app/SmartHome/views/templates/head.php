<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
<title><?=TITLE?></title>

<link rel="apple-touch-icon" sizes="57x57" href="<?=base_url('/public/img/apple-touch-icon-57x57.png')?>">
<link rel="apple-touch-icon" sizes="114x114" href="<?=base_url('/public/img/apple-touch-icon-114x114.png')?>">
<link rel="apple-touch-icon" sizes="72x72" href="<?=base_url('/public/img/apple-touch-icon-72x72.png')?>">
<link rel="apple-touch-icon" sizes="144x144" href="<?=base_url('/public/img/apple-touch-icon-144x144.png')?>">
<link rel="apple-touch-icon" sizes="60x60" href="<?=base_url('/public/img/apple-touch-icon-60x60.png')?>">
<link rel="apple-touch-icon" sizes="120x120" href="<?=base_url('/public/img/apple-touch-icon-120x120.png')?>">
<link rel="apple-touch-icon" sizes="76x76" href="<?=base_url('/public/img/apple-touch-icon-76x76.png')?>">
<link rel="apple-touch-icon" sizes="152x152" href="<?=base_url('/public/img/apple-touch-icon-152x152.png')?>">
<link rel="apple-touch-icon" sizes="180x180" href="<?=base_url('/public/img/apple-touch-icon-180x180.png')?>">
<link rel="shortcut icon" href="<?=base_url('/public/img/favicon.ico')?>">
<link rel="icon" type="image/png" href="<?=base_url('/public/img/favicon-192x192.png" sizes="192x192')?>">
<link rel="icon" type="image/png" href="<?=base_url('/public/img/favicon-160x160.png" sizes="160x160')?>">
<link rel="icon" type="image/png" href="<?=base_url('/public/img/favicon-96x96.png" sizes="96x96')?>">
<link rel="icon" type="image/png" href="<?=base_url('/public/img/favicon-16x16.png" sizes="16x16')?>">
<link rel="icon" type="image/png" href="<?=base_url('/public/img/favicon-32x32.png" sizes="32x32')?>">
<meta name="msapplication-TileColor" content="#f6754f">
<meta name="msapplication-TileImage" content="<?=base_url('/public/img/mstile-144x144.png')?>">
<meta name="msapplication-config" content="<?=base_url('/public/img/browserconfig.xml')?>">

<link rel="stylesheet" href="<?=base_url('public/assets/bootstrap/css/bootstrap.min.css')?>" />
<link rel="stylesheet" href="<?=base_url('public/assets/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')?>" />
<link rel="stylesheet" href="<?=base_url('public/assets/flaticon/flaticon.css')?>" />
<link rel="stylesheet" href="<?=base_url('public/css/custom.css')?>" />
