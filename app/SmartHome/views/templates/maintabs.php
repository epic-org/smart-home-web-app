<h2 class="row">
	<ul class="nav nav-tabs" role="tablist">
        <li class="<?= (strpos(uri_string(), $href['devices']) !== FALSE) ? ' active' : '' ?>">
            <a href="<?=site_url($href['devices'] . '/' . $this->session->userdata('room_id'))?>"><span class="glyphicon glyphicon-flash label-medium"></span><span class="hidden-xs">&nbsp;Devices</span></a>
        </li>
        <li class="<?=NORMAL_ONLY?> <?= (strpos(uri_string(), $href['presets']) !== FALSE) ? ' active' : '' ?>">
            <a href="<?=site_url($href['presets'] . '/' . $this->session->userdata('room_id'))?>"><span class="glyphicon glyphicon-tasks label-medium"></span><span class="hidden-xs">&nbsp;Presets</span></a>
        </li>
        <li class="<?=MASTER_ONLY?> <?= (strpos(uri_string(), $href['auto']) !== FALSE) ? ' active' : '' ?>">
            <a href="<?=site_url($href['auto'] . '/' . $this->session->userdata('room_id'))?>"><span class="glyphicon glyphicon-cog label-medium"></span><span class="hidden-xs">&nbsp;Auto</span></a>
        </li>
        <li class="<?=MASTER_ONLY?> <?= (strpos(uri_string(), $href['schedule']) !== FALSE) ? ' active' : '' ?>">
            <a href="<?=site_url($href['schedule'] . '/' . $this->session->userdata('room_id'))?>"><span class="glyphicon glyphicon-time label-medium"></span><span class="hidden-xs">&nbsp;Schedule</span></a>
        </li>
	</ul>
</h2>
