<div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="settingsModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h3 id="settingsModalLabel" class="modal-title">Account Settings</h3>
			</div> <!-- /.modal-header -->
			<div class="modal-body">
				<div id="settings-alert" class="alert alert-info" role="alert">Please only fill in the fields that you wish to change. <strong>Empty fields will not be saved.</strong></div>
				<?=form_open($json['update_settings'], array('role' => 'form', 'id' => 'accountSettings'))?>
					<div class="panel-group settings-accordion" id="accordion">
                        <?=$settings['change_pin']?>
                        <?=$settings['change_password']?>
                        <?=$settings['change_email']?>
					</div> <!-- /#accordion -->
					<div class="modal-footer">
						<button type="submit" data-loading-text="..." id="settings-submit" class="btn btn-primary" name="submit">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div> <!-- /-submit -->
				</form>
			</div> <!-- /.modal-body -->
		</div> <!-- /.modal-header -->
	</div> <!-- /.modal-dialog -->
</div> <!-- /#settingsModal -->

