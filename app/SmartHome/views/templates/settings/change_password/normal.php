<div class=" normal-group">
    <div class="bg-user account-heading">
        Normal
    </div> <!-- /.bg-user account-heading -->
    <div class="normal-body">
        <div class="form-group">
            <label for="normal-current-password">Current Password</label>
            <input name="normal_password[master_password]" type="password" class="form-control" id="normal-current-password" placeholder="Enter current password">
        </div> <!-- /.form-group -->
        <div class="form-group">
            <label for="normal-new-password">New Password</label>
            <input name="normal_password[password]" type="password" class="form-control" id="normal-new-password" placeholder="Enter new password">
        </div> <!-- /.form-group -->
        <div class="form-group">
            <label for="normal-re-password">Reenter Password</label>
            <input name="normal_password[re_password]" type="password" class="form-control" id="normal-re-password" placeholder="Please reenter new password">
        </div> <!-- /.form-group -->
    </div> <!-- /.normal-body -->
</div> <!-- /. normal-group -->

