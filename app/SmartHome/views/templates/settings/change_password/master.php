<div class="master-group">
    <div class="bg-user account-heading">
        Master
    </div> <!-- /.bg-user account-heading -->
    <div class="master-body">
        <div class="form-group">
            <label for="master-current-password">Current Password</label>
            <input name="master_password[current]" type="password" class="form-control" id="master-current-password" placeholder="Enter current password">
        </div> <!-- /.form-group -->
        <div class="form-group">
            <label for="master-new-password">New Password</label>
            <input name="master_password[password]" type="password" class="form-control" id="master-new-password" placeholder="Enter new password">
        </div> <!-- /.form-group -->
        <div class="form-group">
            <label for="master-re-password">Reenter Password</label>
            <input name="master_password[re_password]" type="password" class="form-control" id="master-re-password" placeholder="Please reenter new password">
        </div> <!-- /.form-group -->
    </div> <!-- /.master-body -->
</div> <!-- /.master-group -->

