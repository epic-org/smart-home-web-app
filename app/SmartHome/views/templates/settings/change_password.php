<div class="panel panel-info">
    <a href="#collapsePassword" data-toggle="collapse" data-parent="#accordion" onclick="iconToggle(this)">
        <div class="panel-heading">
            <div class="panel-title label-medium"><span class="glyphicon glyphicon-collapse glyphicon-expand"></span>&nbsp;Change <span class="hidden-xs">Remote Access</span> Password <span class="glyphicon glyphicon-lock end-icon pull-right"></span></div>
        </div> <!-- /.panel-heading -->
    </a>
</div> <!-- /.panel .panel-info -->
<div id="collapsePassword" class="panel-collapse collapse">
    <div class="panel-body">
        <?=$change_password['master']?>
        <hr />
        <?=$change_password['normal']?>
    </div> <!-- /.panel-body -->
</div> <!-- /#collapsePassword -->

