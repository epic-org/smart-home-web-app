<div class="master-group">
    <div class="bg-user account-heading">
        Master
    </div> <!-- /.bg-user account-heading -->
    <div class="master-body">
        <div class="form-group">
            <label for="master-current-pin">Current PIN</label>
            <input name="master_pin[current]" type="password" pattern="[0-9]*" inputmode="numeric" class="form-control" id="master-current-pin" placeholder="Enter current PIN">
        </div> <!--/.form-group -->
        <div class="form-group">
            <label for="master-new-pin">New PIN</label>
            <input name="master_pin[pin]" type="password" pattern="[0-9]*" inputmode="numeric" class="form-control" id="master-new-pin" placeholder="Enter new PIN">
        </div> <!--/.form-group -->
        <div class="form-group">
            <label for="master-re-pin">Reenter PIN</label>
            <input name="master_pin[re_pin]" type="password" pattern="[0-9]*" inputmode="numeric" class="form-control" id="master-re-pin" placeholder="Please reenter new PIN">
        </div> <!--/.form-group -->
    </div> <!-- /.master-body -->
</div> <!-- /.master-group -->

