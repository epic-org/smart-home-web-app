<div class="normal-group">
    <div class="bg-user account-heading">
        Normal
    </div> <!-- /.bg-user account-heading -->
    <div class=" normal-body">
        <div class="form-group">
            <label for="master-current-pin">Master PIN</label>
            <input name="normal_pin[master_pin]" type="password" pattern="[0-9]*" inputmode="numeric" class="form-control" id="master-current-pin" placeholder="Enter current PIN">
        </div> <!-- /.form-group -->
        <div class="form-group">
            <label for="normal-new-pin">New PIN for Normal User</label>
            <input name="normal_pin[pin]" type="password" pattern="[0-9]*" inputmode="numeric" class="form-control" id="normal-new-pin" placeholder="Enter new PIN">
        </div> <!-- /.form-group -->
        <div class="form-group">
            <label for="normal-re-pin">Reenter PIN for Normal User</label>
            <input name="normal_pin[re_pin]" type="password" pattern="[0-9]*" inputmode="numeric" class="form-control" id="normal-re-pin" placeholder="Please reenter new PIN">
        </div> <!-- /.form-group -->
    </div> <!-- /.normal-body -->
</div> <!-- /.normal-group -->

