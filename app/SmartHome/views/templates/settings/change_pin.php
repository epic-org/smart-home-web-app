<div class="panel panel-info">
    <a href="#collapsePin" data-toggle="collapse" data-parent="#accordion" onclick="iconToggle(this)">
        <div class="panel-heading">
            <div class="panel-title label-medium"><span class="glyphicon glyphicon-collapse glyphicon-expand"></span>&nbsp;Change Pin <span class="glyphicon glyphicon-th end-icon pull-right"></span></div>
        </div> <!-- /.panel-heading -->
    </a>
</div> <!-- /.panel .panel-info -->
<div id="collapsePin" class="panel-collapse collapse">
    <div class="panel-body">
        <?=$change_pin['master']?>
        <hr />
        <?=$change_pin['normal']?>
    </div> <!-- /.panel-body -->
</div> <!-- /#collapsePin -->

