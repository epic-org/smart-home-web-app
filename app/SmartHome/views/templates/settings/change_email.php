<div class="panel panel-info">
    <a href="#collapseEmail" data-toggle="collapse" data-parent="#accordion" onclick="iconToggle(this)">
        <div class="panel-heading">
            <div class="panel-title label-medium"><span class="glyphicon glyphicon-collapse glyphicon-expand"></span>&nbsp;Change <span class="hidden-xs">Recovery</span> Email<span class="glyphicon glyphicon-envelope end-icon pull-right"></span></div>
        </div> <!-- /.panel-heading -->
    </a>
</div> <!-- /.panel .panel-info -->
<div id="collapseEmail" class="panel-collapse collapse">
    <div class="panel-body">
        <div class="email-group">
            <div class="email-body">
                <div class="form-group">
                    <label for="email-current-password">Current Password</label>
                    <input name="master_email[password]" type="password" class="form-control" id="email-current-password" placeholder="Enter current password" aria-describedby="emailhelpBlock">
                    <span id="emailhelpBlock" class="help-block">Please enter your currently logged in account password for verification purposes.</span>
                </div> <!-- /.form-group -->
                <div class="form-group">
                    <label for="new-email">New Email</label>
                    <input name="master_email[email]" type="email" class="form-control" id="new-email" placeholder="Enter new email">
                </div> <!-- /.form-group -->
                <div class="form-group">
                    <label for="re-email">Reenter Email</label>
                    <input name="master_email[re_email]" type="email" class="form-control" id="re-email" placeholder="Please reenter new email">
                </div> <!-- /.form-group -->
            </div> <!-- /.email-body -->
        </div> <!-- /.email-group -->
    </div> <!-- /.panel-body -->
</div> <!-- /#collapseEmail -->

