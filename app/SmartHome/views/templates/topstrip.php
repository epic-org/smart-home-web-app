<div class="row">
	<div class="col-xs-4 col-md-4"><button type="button" class="btn bg-user <?=MASTER_HIDE?>" data-toggle="modal" data-target="#settingsModal"><span class="glyphicon glyphicon-user"></span>&nbsp;<strong>Account <span class="hidden-xs">Settings</span></strong></button></div>
    <div class="col-xs-4 col-md-4 col-xs-offset-4 col-md-offset-4 text-right"><a href="<?=site_url($href['logout'])?>" class="btn btn-danger"><span class="glyphicon glyphicon-log-out"></span>&nbsp;<strong>Logout</strong></a></div>
</div>
