<!DOCTYPE html>
<html>
	<head>
    <?=$head?>
	</head>
	<body>
		<div class="container">
			<?=$topstrip?>
            <?=$room_name?>
			<?=$maintabs?>
			<div class="row row-normal panel panel-separate">
				<a href="<?=site_url($href['new_schedule'] . '/' . $this->session->userdata('room_id'))?>">
					<div class="col-xs-2 col-md-2"><button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-edit text-info" aria-hidden="true"></span></button></div>
					<div class="col-xs-10 col-md-10 label-medium auto-check new-title">New Schedule</div>
				</a>
			</div> <!-- /row row-normal panel panel-danger -->
            <?php if ($schedules !== FALSE): ?>
            <?php foreach($schedules as $sch): ?>
            <div class="row row-normal panel panel-default" id="panel-<?=$sch->id?>">
				<div class="col-xs-2 col-md-2"><a href="<?=site_url($href['edit_schedule'] . '/' . $sch->id)?>"><button class="btn btn-default" type="button"><span class="glyphicon glyphicon-wrench"></span><span class="sr-only">Edit</span></button></a></div>
				<div class="checkbox col-xs-7 col-md-8 label-medium no-margin">
                <input class="schedule-check toggle-enable" type="checkbox" data-id="<?=$sch->id?>" id="enable-<?=$sch->id?>" name="<?=$sch->id?>" value="1" <?= $sch->enabled ? 'checked' : '' ?>>
					<div class="check-label">
	                    <div class="col-md-4 label-medium"><label for="enable-<?=$sch->id?>"><?=$sch->name?></label></div>
	                    <div class="hidden-xs col-md-3 label-medium"><label for="<?=$sch->id?>"><?=format_from($sch->time, 'H:i:s', 'h:i A')?></label></div>
	                    <div class="hidden-xs col-md-5 label-medium"><label for="<?=$sch->id?>"><?= $sch->day == 0 ? format_from($sch->date, 'Y-m-d', 'j M Y') : get_active_days($sch->day)?></label></div>
					</div>
				</div>
                <div class="col-xs-3 col-md-2"><a href="#"><button class="btn btn-danger btn-lg" type="button" data-loading-text="..." onclick="deleteById(<?=$sch->id?>)"><span class="glyphicon glyphicon-trash"></span><span class="sr-only">Delete</span></button></a></div>
				<div class="row">
					<div class="visible-xs-block col-xs-4 label-medium"><label for="<?=$sch->id?>"><?=format_from($sch->time, 'H:i:s', 'h:i A')?></label></div>
					<div class="visible-xs-block col-xs-8 label-medium"><label for="<?=$sch->id?>"><?= $sch->day == 0 ? format_from($sch->date, 'Y-m-d', 'j M Y') : get_active_days($sch->day)?></label></div>
				</div>
			</div> <!-- /.row .row-normal .panel .panel-default -->
            <?php endforeach ?>
            <?php endif ?>
		</div> <!-- /container -->
        <?=form_hidden('delete', $ajax['delete_schedule'])?>
        <?=form_hidden('toggle_enable', $ajax['toggle_schedule_task'])?>
        <?=$hidden?>
		<?=$settings?>
        <?=$js?>
	</body>
</html>
