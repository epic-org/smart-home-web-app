<?php if ($actuators !== FALSE): ?>
<div class="panel panel-info">
	<a href="#collapseOne" data-toggle="collapse" data-parent="#accordion" onclick="iconToggle(this)">
		<div class="panel-heading">
			<div class="panel-title label-medium"><span class="glyphicon glyphicon-collapse glyphicon-expand"></span>&nbsp;Lights<span class="flaticon flaticon-bright4 pull-right"></span></div>
		</div> <!-- /.panel-heading -->
	</a>
</div> <!-- /.panel .panel-default -->
<div id="collapseOne" class="panel-collapse collapse">
	<div class="panel-body">
        <?php $i = 0; ?>
        <?php for ($index; $index < $count; $index++): ?>
        <?php $id = $actuators[$i]->id ?>
        <?php $name = $actuators[$i]->name ?>
        <?php $enabled = isset($state[$id]['enabled']) ? $state[$id]['enabled'] : FALSE ?>
        <?php $value = isset($state[$id]['value']) ? $state[$id]['value'] : 0 ?>
        <?php $populated = isset($_actuators) ? : FALSE ?>
        <?=form_hidden("actuators[{$index}][actuator_id]", $id)?>
        <?=form_hidden("actuators[{$index}][type]", $actuators[$i]->type)?>
        <div id="panel-<?=$id?>" class="row panel panel-default <?=isset($_actuators[$index]['enabled']) == FALSE ? ($enabled ? 'success':'') : 'success' ?>">
			<div class="panel-body">
				<div class="checkbox">
                    <div class="col-xs-1 col-md-2">
                        <input id="en-<?=$id?>" class="en-check" type="checkbox" value="1" <?=isset($_actuators[$index]['enabled']) == FALSE ? ($enabled ? 'checked':'') : 'checked' ?> name="actuators[<?=$index?>][enabled]" onclick="enToggle(<?=$id?>)">
                    </div> <!-- /.col-xs-1 col-md-2 -->
                <h4 class="col-xs-4 col-md-4"><label><?=$name?></label></h4>
                </div> <!-- /.checkbox -->
				<div class="col-xs-4 col-md-4"><input name="actuators[<?=$index?>][actuator_value]" class="slider slider-preset" type="range" id="slider-<?=$id?>" min="0" max="255" value="<?= $populated ? $_actuators[$index]['actuator_value'] : $value?>"></div>
				<div class="col-xs-2 col-md-2">
                    <button id="<?=$id?>" type="button" class="btn btn-default btn-preset-power" data-init-value="<?=$populated == FALSE ? ($enabled ? ($value == ON ? 'ON':'OFF'):'OFF') : (isset($_actuators[$index]['enabled']) ? ($_actuators[$index]['on'] == ON ? 'ON' : 'OFF') : 'OFF')?>" data-value-on="<?=ON?>" data-value-off="<?=OFF?>">Off</button> <!-- Off by default -->
                    <input type="hidden" value="<?=OFF?>" name="actuators[<?=$index?>][on]">
				</div> <!-- /.col-xs-3 col-xs-offset-5 col-md-2 col-md-offset-6-->
			</div> <!-- /.panel-body -->
		</div> <!-- /.row panel .panel-default -->
        <?php $i++; ?>
        <?php endfor ?>
	</div> <!-- /.panel-body -->
</div> <!-- #collapseOne -->
<?php endif ?>
