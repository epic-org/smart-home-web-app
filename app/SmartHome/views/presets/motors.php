<?php if ($actuators !== FALSE): ?>
<div class="panel panel-info">
	<a href="#collapseTwo" data-toggle="collapse" data-parent="#accordion" onclick="iconToggle(this)">
		<div class="panel-heading">
            <div class="panel-title label-medium"><span class="glyphicon glyphicon-collapse glyphicon-expand"></span>&nbsp;<abbr title="e.g. Curtains, TV panel motors">Motorized Devices</abbr><span class="flaticon flaticon-computer32 pull-right"></span></div>
		</div> <!-- /.panel-heading -->
	</a>
</div> <!-- /.panel .panel-info -->
<div id="collapseTwo" class="panel-collapse collapse">
	<div class="panel-body">
        <?php $i = 0; ?>
        <?php for ($index; $index < $count; $index++): ?>
        <?php $id = $actuators[$i]->id ?>
        <?php $name = $actuators[$i]->name ?>
        <?php $enabled = isset($state[$id]['enabled']) ? $state[$id]['enabled'] : FALSE ?>
        <?php $value = isset($state[$id]['value']) ? $state[$id]['value'] : 0 ?>
        <?php $populated = isset($_actuators) ? : FALSE ?>
        <?=form_hidden("actuators[{$index}][actuator_id]", $id)?>
        <?=form_hidden("actuators[{$index}][type]", $actuators[$i]->type)?>
        <div id="panel-<?=$id?>" class="row panel panel-default panel-actuator <?=isset($_actuators[$index]['enabled']) ? 'success' : ($enabled ? 'success':'')?>">
			<div class="panel-body">
				<div class="checkbox">
                    <div class="col-xs-1 col-md-2">
                        <input id="en-<?=$id?>" class="en-check" type="checkbox" value="1" <?=isset($_actuators[$index]['enabled']) ? 'checked' : ($enabled ? 'checked':'') ?> name="actuators[<?=$index?>][enabled]" onclick="enToggle(<?=$id?>)">
                    </div> <!-- /.col-xs-1 col-md-2 -->
                    <h4 class="col-xs-4 col-md-4"><label for="en-<?=$id?>"><?=$name?></label></h4>
                </div> <!-- /.checkbox -->
				<div class="col-xs-3 col-xs-offset-4 col-md-2 col-md-offset-4">
					<button id="<?=$id?>" type="button" class="btn btn-default btn-preset-motor" data-init-value="<?=$populated == FALSE ? ($enabled ? ($value == CLOSE ? 'CLOSE':'OPEN'):'OPEN') : (isset($_actuators[$index]['enabled']) ? ($_actuators[$index]['actuator_value'] == CLOSE ? 'CLOSE':'OPEN') : 'OPEN')?>" data-value-on="<?=OPEN?>" data-value-off="<?=CLOSE?>">Open</button> <!-- Open by default -->
                    <input type="hidden" value="<?=OPEN?>" name="actuators[<?=$index?>][actuator_value]">
				</div> <!-- /.col-xs-3 col-xs-offset-5 col-md-2 col-md-offset-6 -->
			</div> <!-- /.panel-body -->
		</div> <!-- /.row panel panel-default -->
        <?php $i++; ?>
        <?php endfor  ?>
	</div> <!-- /.panel-body -->
</div> <!-- /#collapseTwo -->
<?php endif ?>
