<?php if ($actuators !== FALSE): ?>
<div class="panel panel-info">
	<a href="#collapseThree" data-toggle="collapse" data-parent="#accordion" onclick="iconToggle(this)">
		<div class="panel-heading">
			<div class="panel-title label-medium"><span class="glyphicon glyphicon-collapse glyphicon-expand"></span>&nbsp;Air Cooling Systems<span class="flaticon flaticon-snow42 pull-right"></span></div>
		</div> <!-- /.panel-heading -->
	</a>
</div> <!-- /#collapseThree -->
<div id="collapseThree" class="panel-collapse collapse">
	<div class="panel-body">
        <?php $i = 0; ?>
        <?php for ($index; $index < $count; $index++): ?>
        <?php $id = $actuators[$i]->id ?>
        <?php $name = $actuators[$i]->name ?>
        <?php $enabled = isset($state[$id]['enabled']) ? $state[$id]['enabled'] : FALSE ?>
        <?php $value = isset($state[$id]['value']) ? $state[$id]['value'] : 0 ?>
        <?php $populated = isset($_actuators) ? : FALSE ?>
        <?=form_hidden("actuators[{$index}][actuator_id]", $id)?>
        <?=form_hidden("actuators[{$index}][type]", $actuators[$i]->type)?>
        <div id="panel-<?=$id?>" class="row panel panel-default panel-actuator <?=isset($_actuators[$index]['enabled']) ? 'success' : ($enabled ? 'success':'') ?>">
			<div class="panel-body">
				<div class="checkbox">
                    <div class="col-xs-1 col-md-2">
                        <input id="en-<?=$id?>" class="en-check" type="checkbox" value="1" <?=isset($_actuators[$index]['enabled']) ? 'checked' : ($enabled ? 'checked':'') ?> name="actuators[<?=$index?>][enabled]" onclick="enToggle(<?=$id?>)">
                    </div> <!-- /.col-xs-1 col-md-2 -->
                    <h4 class="col-xs-4 col-md-4"><label for="en-<?=$id?>"><?=$name?></label></h4>
                </div> <!-- /.checkbox -->
				<div class="col-xs-3 col-xs-offset-4 col-md-2 col-md-offset-4">
					<button id="<?=$id?>" type="button" class="btn btn-default btn-preset-power" data-init-value="<?=$populated == FALSE ? ($enabled ? ($value == ON ? 'ON':'OFF'):'OFF') : (isset($_actuators[$index]['actuator_value']) ? ($_actuators[$index]['actuator_value'] == ON ? 'ON':'OFF') : 'OFF') ?>" data-value-on="<?=ON?>" data-value-off="<?=OFF?>">Off</button> <!-- Off by default -->
                    <input type="hidden" value="<?=OFF?>" name="actuators[<?=$index?>][actuator_value]">
				</div> <!-- /.col-xs-3 col-xs-offset-5 col-md-2 col-md-offset-6 -->
			</div> <!-- /.panel-body -->
		</div> <!-- /.row panel panel-default -->
        <?php $i++; ?>
        <?php endfor ?>
	</div> <!-- /.panel-body -->
</div> <!-- /#collapseThree -->
<?php endif ?>
