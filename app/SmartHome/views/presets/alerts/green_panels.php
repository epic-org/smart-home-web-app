<div class="alert alert-info label-small alert-preset" role="alert">
	Only <strong class="text-success">green panels</strong> will be saved for the Preset.
</div>