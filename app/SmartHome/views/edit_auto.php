<!DOCTYPE html>
<html>
	<head>
    <?=$head?>
	</head>
	<body>
		<div class="container">
			<?=$topstrip?>
			<div class="page-header">
            <h1><span id="room-name"></span><br /><small>Edit Automation</small></h1>
			</div>
			<?=form_open(current_url(), array('role' => 'form'))?>
				<div class="row well">
                    <?=format_error(validation_errors())?>
                    <?=$auto['step_1']?>
                    <?=$auto['step_2']?>
                    <?=$auto['step_3']?>
                    <?=$auto['step_4']?>
                    <?=$auto['step_5']?>
					<div class="row row-submit">
						<button type="submit" class="btn btn-primary btn-lg" name="submit" data-loading-text="Saving..."><span class="glyphicon glyphicon-save"></span>&nbsp;Save</button>
						<a href="<?=site_url($href['auto'] . '/' . $this->session->userdata('room_id'))?>" class="btn btn-default btn-lg">Cancel</a>
					</div> <!-- /row-submit -->
				</div> <!-- /.row .well -->
			</form>
		</div> <!-- /container -->
        <?=form_hidden('get_reading', $json['sensor_reading'])?>
        <?=$hidden?>
		<?=$settings?>
        <?=$js?>
		<script type="text/javascript">
			$(document).ready(function() {
			    $('#custom-checkbox').trigger('click');
			});
		</script>
	</body>
</html>
