<!DOCTYPE html>
<html>
    <head>
    <?=$head?>
    </head>
    <body>
        <div class="container">
            <h1>Smart Home!</h1>
            <?=format_error(validation_errors())?>
            <?=form_open('login')?>
            <?php if ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <strong><?=$this->session->flashdata('error')?></strong> Please try again.
            </div>
            <?php endif ?>
            <div class="form-group">
                <label for="login-id">User</label>
                <select name="account_type" id="account_type" class="form-control">
                    <option value="<?=MASTER?>">Master</option>
                    <option value="<?=NORMAL?>">Normal</option>
                </select>
            </div>
            <div class="form-group">
                <label for="login-id">Password</label>
                <input type="password" class="form-control" value="" name="password" placeholder="Enter password">
            </div>
            <input type="submit" class="btn btn-primary btn-lg" value="Login" id="login-submit" name="submit" disabled="disabled">
            <button type="button" class="btn btn-warning btn-lg" data-toggle="modal" data-target="#recoveryModal"><span class="glyphicon glyphicon-lock"></span>&nbsp;Forgot Password?</button>
            
            <!-- <h4 id="alert-enable" class="alert alert-danger" role="alert"><strong>Attention!</strong> Please enable Cookies and Javascript! &nbsp;<span id="login-info" type="button" class="glyphicon glyphicon-question-sign" data-toggle="popover" title="<h4 class='login-popover-title'>What's this? <button type='button' class='close' onclick='closeLoginPopover()'><span aria-hidden='true'>&times;</span><span class='sr-only'>Close</span></button></h4>" data-content="<p>Cookies/Javascript are currently disabled on your browser. To proceed, you must enable Cookies &amp; Javascript.</p> <u>More info (<strong>external links</strong>):</u><ul><li><a href='http://www.aboutcookies.org/default.aspx?page=5' target='_blank'>Cookies</a><small>&nbsp;(opens in new tab)</small></li><li><a href='http://www.enable-javascript.com/' target='_blank'>Javascript</a><small>&nbsp;(opens in new tab)</small></li></ul><strong>Still need help?</strong> Give us a call."></span></h4> -->
            <?=$alerts['login']?>

            <?=form_close()?>
        </div>
        <!-- Begin Modal -->
        <div class="modal fade" id="recoveryModal" tabindex="-1" role="dialog" aria-labelledby="recoveryModalLabel" aria-hidden="true">
            <?=form_open(current_url(), array('role' => 'form'))?>
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="recoveryModalLabel">Forgot Password</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="email">Recovery Email</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Enter email that is registered with Smart Home">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" name="forgot_password" value="submit">Submit</button>
                    </div>
                </div> <!-- /.modal-content -->
            </div> <!-- /.modal-dialog -->
            <?=form_close()?>
        </div> <!-- /#recoveryModal -->
        <!-- End Modal -->
        <?=$js?>
    </body>
</html>
