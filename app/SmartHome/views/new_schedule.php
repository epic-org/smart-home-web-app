<!DOCTYPE html>
<html>
	<head>
    <?=$head?>
	</head>
	<body>
		<div class="container">
			<?=$topstrip?>
			<div class="page-header">
            <h1><span id="room-name"></span><br /><small>New Schedule</small></h1>
			</div>
			<?=form_open(current_url(), array('role' => 'form'))?>
                <?=format_error(validation_errors())?>
                <?=$schedule['name']?>
				<div class="row well">
                    <?=$schedule['time_date']?>
                    <?=$schedule['repeat']?>
                    <?=$schedule['selection']?>
				</div> <!-- /.row .well -->
				<div class="row row-submit">
					<button type="submit" class="btn btn-primary btn-lg" name="submit" data-loading-text="Saving..."><span class="glyphicon glyphicon-save"></span>&nbsp;Save</button>
					<a href="<?=site_url($href['schedule'] . '/' . $this->session->userdata('room_id'))?>" class="btn btn-default btn-lg">Cancel</a>
				</div> <!-- /.row .row-submit -->
			</form>
		</div> <!-- /container -->
        <?=$hidden?>
		<?=$settings?>
        <?=$js?>
	</body>
</html>
