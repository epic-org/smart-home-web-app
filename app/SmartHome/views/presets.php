<!DOCTYPE html>
<html>
    <head>
    <?=$head?>
    </head>
    <body>
        <div class="container">
            <?=$topstrip?>
            <?=$room_name?>
			<?=$maintabs?>
            <?= $this->session->flashdata('error') ? format_error($this->session->flashdata('error')) : '' ?>
            <div class="row row-normal panel panel-separate <?=MASTER_ONLY?>">
				<a class="<?=MASTER_ONLY?>" href="<?=site_url($href['new_preset'] . '/' . $this->session->userdata('room_id'))?>">
					<div class="col-xs-2 col-md-2"><button type="button" class="btn btn-default btn-lg <?=MASTER_ONLY?>"><span class="glyphicon glyphicon-edit text-info" aria-hidden="true"></span></button></div>
					<div class="col-xs-10 col-md-10 label-medium auto-check new-title <?=MASTER_ONLY?>">New Preset</div>
				</a>
			</div> <!-- /row -->
            <?php if ($presets !== FALSE): ?>
            <?php foreach($presets as $preset): ?>
            <div class="row row-normal panel panel-default" id="panel-<?=$preset->id?>" >
	            <div class="col-xs-2 col-md-2"><a class="<?=MASTER_ONLY?>" href="<?=site_url($href['edit_preset'] . '/' . $preset->id)?>"><button class="btn btn-default btn-lg <?=MASTER_ONLY?>" type="button"><span class="glyphicon glyphicon-wrench"></span><span class="sr-only">Edit</span></button></a></div>
                <div data-preset-id="<?=$preset->id?>" class="col-xs-7 col-md-8 label-medium auto-check send-preset"><?=$preset->name?></div>
                <div class="col-xs-3 col-md-2"><a class="<?=MASTER_ONLY?>" href="#"><button class="btn btn-danger btn-lg <?=MASTER_ONLY?>" type="button" data-loading-text="..." onclick="deletePreset(<?=$preset->id?>)"><span class="glyphicon glyphicon-trash"></span><span class="sr-only">Delete</span></button></a></div>
            </div> <!-- /row -->
            <?php endforeach ?>
            <?php endif ?>
        </div> <!-- /container -->
        <?=form_hidden('get_state', $json['get_preset_state'])?>
        <?=form_hidden('delete', $ajax['delete_preset'])?>
        <?=form_hidden('send_command', $ajax['send_command'])?>
        <?=$hidden?>
        <?=$settings?>
        <?=$js?>
    </body>
</html>
