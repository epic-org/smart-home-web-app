<!DOCTYPE html>
<html>
	<head>
    <?=$head?>
	</head>
	<body>
		<div class="container">
            <div class="jumbotron">
	            <?php if ($this->session->flashdata('error')): ?>
		        <div class="alert alert-danger alert-dismissible" role="alert">
		            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		            <strong><?=$this->session->flashdata('error')?></strong>
		        </div> <!-- /.alert .alert-danger .alert-dismissible -->
		        <?php endif ?>
				<h1>Welcome!</h1> <p>Please select a room</p>
				<?php foreach ($rooms as $room) :?>
					<div class="panel panel-default clearfix">
                        <div class="panel-body">
                        <div id="rename-begin-<?=$room->id?>" class="col-xs-2 col-md-2"><button class="btn btn-default btn-stop pull-left <?=MASTER_ONLY?>" type="button" onclick="toggleRename(<?=$room->id?>)"><span class="glyphicon glyphicon-pencil"></span></button></div>
                        <div id="rename-done-<?=$room->id?>" class="col-xs-2 col-md-2" style="display:none"><button class="btn btn-success rename-success" type="button" data-loading-text="..." onclick="rename(<?=$room->id?>)"><span class="glyphicon glyphicon-ok"></span><span class="hidden-xs">&nbsp;Done</span></button></div>
                        <div class="hide-on-rename-<?=$room->id?>">
	                        <a id="a-<?=$room->id?>" href="<?=site_url($href['devices'] . '/' . $room->id)?>">
		                        <div class="col-xs-10 col-md-10">
		                            <h3 id="input-<?=$room->id?>"><?=$room->name?></h3>
		                        </div>
	                        </a>
						</div> <!-- /.hide-on-rename -->
						<div class="show-on-rename-<?=$room->id?>" style="display:none">
			                <h4 class="col-xs-10 col-md-10 new-name">
				                <input id="name-<?=$room->id?>" class="form-control" type="text" value="<?=$room->name?>" placeholder="<?=$room->name?>" />
			                	<?=$alerts['info_rename']?>
		                	</h4>
						</div> <!-- /.show-on-rename -->
                        </div> <!-- /.panel-body -->
					</div> <!-- /.panel .panel-default .clearfix -->
				<?php endforeach ?>
				<div class="row text-right">
					<button type="button" class="btn btn-lg bg-user <?=MASTER_HIDE?>" data-toggle="modal" data-target="#settingsModal"><span class="glyphicon glyphicon-user"></span>&nbsp;Account <span class="hidden-xs">Settings</span></button>
					<a href="<?=site_url($href['logout'])?>" class="btn btn-danger btn-lg"><span class="glyphicon glyphicon-log-out"></span>&nbsp;Logout</a>
				</div> <!-- /.row .text-right -->
			</div> <!-- /.jumbotron -->
		</div> <!-- /.container -->
        <?=form_close()?>
        <?=form_hidden('rename', $ajax['rename_room'])?>
        <?=form_hidden('uri', uri_string())?>
        <?=$hidden?>
		<?=$settings?>
        <?=$js?>
	</body>
</html>
