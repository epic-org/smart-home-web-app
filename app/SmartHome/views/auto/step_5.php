<div class="row">
	<h3><strong class="badge-steps badge-spacing">5</strong>Is this what you want to do?</h3>
	<div class="col-xs-12 col-md-12">
		<blockquote id="default-quote">When it is <strong id="brightness-check">Dark (less than 10%)</strong>, enable the <strong class="preset-check-quote"><?=$presets[0]->name?></strong> preset.</blockquote>
		<blockquote id="custom-quote" style="display:none">When brightness is <strong id="custom-brightness-check">more than <?=$val?>%</strong>, enable the <strong class="preset-check-quote"><?=isset($automation_state) ? $automation_state->preset_name : $presets[0]->name?></strong> preset.</blockquote>
	</div>
</div> <!-- /.row -->
