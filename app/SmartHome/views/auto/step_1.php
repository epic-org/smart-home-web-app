<h3 class="row"><strong class="badge-steps">1</strong>What would you like to call this automation?</h3>
<div class="row row-form-name"><input name="automation_name" type="text" class="form-control input-lg" id="auto-name" placeholder="<?= isset($automation_state) ? $automation_state->automation_name : 'Enter new automation name'?>" autofocus></div>
