<div class="row">
	<h3><strong class="badge-steps">3</strong>What is the condition?</span></h3>
	<div class="col-xs-12 col-md-12">
		<select id="brightness-selection" class="form-control" name="sensor_value">
        <option value="<<?=(10/100) * $sensors[0]->max_value?>">Dark (less than 10%)</option>
			<option value="<<?=(30/100) * $sensors[0]->max_value?>">Slightly Dark (less than 30%)</option>
			<option value="><?=(60/100) * $sensors[0]->max_value?>">Bright (more than 60%)</option>
			<option value="><?=(85/100) * $sensors[0]->max_value?>">Very Bright (more than 85%)</option>
		</select>
	</div>
	<div class="col-xs-12 col-md-12">
		<div class="checkbox custom-spacing">
			<label><input name="custom" value="1" id="custom-checkbox" type="checkbox">Custom condition</label>
		</div>
	</div> <!-- /.col-xs-12 col-md-12 -->
	<div class="row auto-custom-row">
		<div class="col-xs-2 col-md-2"><button id="rule" name="rule" type="button" class="btn btn-success btn-lg btn-block" onclick="ruleToggle()" disabled="disabled"><span id="rule-icon" class="glyphicon glyphicon-chevron-right visible-xs-inline"></span><strong id="rule-text" class="hidden-xs">more than</strong></button></div>
        <input type="hidden" id="more-than" value="<?= isset($automation_state) ? $automation_state->more_than : 1?>" name="more_than" />
		<div class="col-xs-10 col-md-8 col-md-offset-1">
			<!-- This block is kept in case of the need to revert to text input from slider. TODO: Remove before production. 
				<div class="input-group">
				<label class="sr-only" for="custom-value">Custom input</label>
				<input id="custom-value" name="custom-value" type="text" class="form-control input-lg" value="<?=$val?>" disabled="disabled">
				<span class="input-group-addon">%</span>
			</div> -->
			<!-- Changed slider id to #custom-value from #slider -->
			<div class="row">
				<div class="col-xs-2 col-md-2 text-right"><div class="row">Dark</div><div class="row">0%</div></div>
                <div class="col-xs-8 col-md-8">
                    <input id="custom-value" name="sensor_custom_value" class="slider-auto" type="range" min="0" max="<?= isset($automation_state) ? $automation_state->max_value : $sensors[0]->max_value?>" value="<?= isset($automation_state) ? $automation_state->sensor_value : $sensor_value?>">
                </div> <!-- /.col-xs-8 .col-md-8 -->
				<div class="col-xs-2 col-md-2 text-left"><div class="row">Bright</div><div class="row">100%</div></div>
			</div> <!-- /.row -->
		</div> <!-- /.col-xs-10 col-md-8 col-md-offset-1 -->
	</div> <!-- /.row auto-custom-row -->
</div> <!-- /.row -->
