<div class="row">
	<h3><strong class="badge-steps badge-spacing">4</strong>Which preset <span class="hidden-xs">do you want to enable</span>?</h3>
	<div class="col-xs-12 col-md-12">
		<select id="preset-select" name="presets[0][preset_id]" class="form-control">
        <!-- TODO: Tell user to add new preset first if empty? -->
        <?php foreach ($presets as $preset): ?>
        <option value="<?=$preset->id?>" <?= isset($automation_state) ? $preset->id == $automation_state->preset_id ? 'selected' : '' : '' ?>><?=$preset->name?></option>
        <?php endforeach ?>
		</select>
	</div>
</div> <!-- /.row -->
