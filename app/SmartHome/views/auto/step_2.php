<div class="row">
	<h3 class="badge-spacing"><strong class="badge-steps">2</strong>Which sensor?</h3>
	<div class="col-xs-12 col-md-12">
        <select class="form-control select-auto" name="sensors[sensor_id]">
        <?php foreach($sensors as $sensor): ?>
            <option value="<?=$sensor->id?>" <?= isset($automation_state) ? $automation_state->sensor_id == $sensor->id ? 'selected' : '' : '' ?>><?=$sensor->name?></option>
        <?php endforeach ?>
        </select>
    </div>
</div> <!-- /.row -->
<div class="row">
	<div class="col-xs-12 col-md-12">
        <h4>&nbsp;Currently <button class="btn btn-default btn-lg" type="button" id="refreshReading" data-sensor-id="<?=$init_sensor_id?>" data-sensor-max-value="<?=$init_max_value?>">
        <span class="glyphicon glyphicon-refresh"></span>&nbsp;Bright (<span id="sensorValue"><?=$val?></span>%)</button>
        <small class="alert alert-info alert-small"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<strong>Press to refresh</strong></small>
        </h4>
    </div>
</div> <!-- /.row -->
