<?php if ($actuators !== FALSE ): ?>
<div class="panel panel-info">
	<a href="#collapseOne" data-toggle="collapse" data-parent="#accordion" onclick="iconToggle(this)">
		<div class="panel-heading">
			<div class="panel-title label-medium"><span class="glyphicon glyphicon-collapse glyphicon-expand"></span>&nbsp;Lights<span class="flaticon flaticon-bright4 pull-right"></span></div>
		</div> <!-- /.panel-heading -->
	</a>
</div> <!-- /.panel .panel-info -->
<div id="collapseOne" class="panel-collapse panel-open collapse">
	<div class="panel-body">
        <?php foreach($actuators as $light): ?>
        <?php $id = $light->id ;?>
		<div id="panel-<?=$id?>" class="row panel panel-default">
			<div class="panel-body">
                <div id="rename-begin-<?=$id?>" class="col-xs-2 col-md-2"><button class="btn btn-default rename-light <?=MASTER_ONLY?>" type="button" onclick="toggleRename(<?=$id?>)"><span class="glyphicon glyphicon-pencil"></span><span class="sr-only">Rename</span></button></div>
                <div id="rename-done-<?=$id?>" class="col-xs-2 col-md-2" style="display:none"><button class="btn btn-success rename-success" type="button" data-loading-text="..." onclick="rename(<?=$id?>)"><span class="glyphicon glyphicon-ok"></span><span class="hidden-xs">&nbsp;Done</span></button></div>
                <div class="hide-on-rename-<?=$id?>">
	                <h4 id="input-<?=$id?>" class="col-xs-3 col-md-3 name-light"><?=$light->name?></h4>
                    <div class="col-xs-4 col-md-4"><input class="slider send-message" data-actuator-id="<?=$id?>" data-signal-value="SLIDER" type="range" min="0" max="255" value="255"></div>
    				<div class="col-xs-3 col-md-2">
    	                <button class="btn btn-primary btn-lg send-message" data-actuator-id="<?=$id?>" data-signal-value="<?=ON?>" type="button">On</button>
    	                <button class="btn btn-default btn-lg send-message" data-actuator-id="<?=$id?>" data-signal-value="<?=OFF?>" type="button">Off</button>
    				</div>
				</div> <!-- /.hide-on-rename -->
				<div class="show-on-rename-<?=$id?>" style="display:none">
	                <h4 class="col-xs-10 col-md-10 new-name">
		                <input id="name-<?=$id?>" class="form-control" type="text" value="<?=$light->name?>" placeholder="<?=$light->name?>" />
	                	<?=$alerts['info_rename']?>
                	</h4>
				</div> <!-- /.show-on-rename -->
			</div> <!-- /.panel-body -->
		</div> <!-- /.row panel .panel-default -->
        <?php endforeach ?>
	</div> <!-- /.panel-body -->
</div> <!-- /#collapseOne -->
<?php endif ?>
