<?php if ($actuators !== FALSE): ?>
<div class="panel panel-info">
	<a href="#collapseTwo" data-toggle="collapse" data-parent="#accordion" onclick="iconToggle(this)">
		<div class="panel-heading">
            <div class="panel-title label-medium"><span class="glyphicon glyphicon-collapse glyphicon-expand"></span>&nbsp;<abbr title="e.g. Curtains, TV panel motors">Motorized Devices</abbr><span class="flaticon flaticon-computer32 pull-right"></span></div>
		</div>
	</a>
</div> <!-- /.panel .panel-info -->
<div id="collapseTwo" class="panel-collapse panel-open collapse">
	<div class="panel-body">
        <?php foreach($actuators as $motor): ?>
        <?php $id = $motor->id; ?>
		<div id="panel-<?=$id?>" class="row panel panel-default">
			<div class="panel-body">
                <div id="rename-begin-<?=$id?>" class="col-xs-2 col-md-2 curtain-icon"><button class="btn btn-default rename-curtain <?=MASTER_ONLY?>" type="button" onclick="toggleRename(<?=$id?>)"><span class="glyphicon glyphicon-pencil"></span><span class="sr-only">Rename</span></button></div>
                <div id="rename-done-<?=$id?>" class="col-xs-2 col-md-2" style="display:none"><button class="btn btn-success rename-success" type="button" data-loading-text="..." onclick="rename(<?=$id?>)"><span class="glyphicon glyphicon-ok"></span><span class="hidden-xs">&nbsp;Done</span></button></div>
                <div class="hide-on-rename-<?=$id?>">
	                <h4 id="input-<?=$id?>" class="col-xs-5 col-md-4 curtain-name"><?=$motor->name?></h4>
					<div class="col-xs-5 col-md-6">
						<div class="row">
							<div class="col-xs-6 col-md-6">
	                        <div class="row"><button class="btn btn-default send-message" data-actuator-id="<?=$id?>" data-signal-value="<?=UP?>" type="button"><span class="glyphicon glyphicon-chevron-up"></span><span class="sr-only">Up</span></button></div>
								<div class="row btn-stop"><button class="btn btn-default send-message" data-actuator-id="<?=$id?>" data-signal-value="<?=DOWN?>" type="button"><span class="glyphicon glyphicon-chevron-down"></span><span class="sr-only">Down</span></button></div>
							</div> <!-- /.col-xs-6 .col-md-6 -->
							<div class="col-xs-6 col-md-6">
								<button class="btn btn-info btn-lg btn-stop send-message" data-actuator-id="<?=$id?>" data-signal-value="<?=STOP?>" type="button"><span class="glyphicon glyphicon-stop"></span><span class="sr-only">Stop</span></button>
							</div> <!-- /.col-xs-6 .col-md-6 -->
						</div> <!-- /.row -->
					</div> <!-- /.col-xs-5 .col-md-6 -->
				</div> <!-- .hide-on-rename -->
				<div class="show-on-rename-<?=$id?>" style="display:none">
	                <h4 class="col-xs-10 col-md-10 new-name">
		                <input id="name-<?=$id?>" class="form-control" type="text" value="<?=$motor->name?>" placeholder="<?=$motor->name?>" />
	                	<?=$alerts['info_rename']?>
                	</h4>
				</div> <!-- /.show-on-rename -->
			</div> <!-- /.panel-body -->
		</div> <!-- /.row panel .panel-default -->
        <?php endforeach ?>
	</div> <!-- /.panel-body -->
</div> <!-- /#collapseTwo -->
<?php endif ?>
