<!DOCTYPE html>
<html>
	<head>
    <?=$head?>
	</head>
	<body>
		<div class="container">
			<?=$topstrip?>
            <?=$room_name?>
			<?=$maintabs?>
			<div class="row row-normal panel panel-separate">
				<a href="<?=site_url($href['new_auto'] . '/' . $this->session->userdata('room_id'))?>">
					<div class="col-xs-2 col-md-2"><button type="button" class="btn btn-default btn-lg"><span class="glyphicon glyphicon-edit text-info" aria-hidden="true"></span></button></div>
					<div class="col-xs-10 col-md-10 label-medium auto-check new-title">New Automation</div>
				</a>
			</div> <!-- /.row .row-normal .panel .panel-danger .panel-separate -->
            <?php foreach($automations as $auto): ?>
            <div id="panel-<?=$auto->id?>" class="row row-normal panel panel-default">
				<div class="col-xs-2 col-md-2"><a href="<?=site_url($href['edit_auto'] . '/' . $auto->id)?>"><button class="btn btn-default" type="button"><span class="glyphicon glyphicon-wrench"></span><span class="sr-only">Edit</span></button></a></div>
				<div class="checkbox col-xs-7 col-md-8 label-medium no-margin">
					<label>
                        <input <?= $auto->enabled ? 'checked':'' ?> data-id="<?=$auto->id?>" class="auto-check toggle-enable" type="checkbox" id="enabled-<?=$auto->id?>" name="enabled"><div class="check-label"><?=$auto->name?></div>
					</label>
				</div>
                <div class="col-xs-3 col-md-2"><a href="#"><button  class="btn btn-danger btn-lg" type="button" data-loading-text="..." onclick="deleteById(<?=$auto->id?>)"><span class="glyphicon glyphicon-trash"></span><span class="sr-only">Delete</span></button></a></div>
			</div> <!-- /.row .row-normal .panel .panel-default -->
            <?php endforeach ?>
		</div> <!-- /.container -->
        <?=form_hidden('delete', $ajax['delete_automation'])?>
        <?=form_hidden('toggle_enable', $ajax['toggle_automation'])?>
        <?=$hidden?>
		<?=$settings?>
        <?=$js?>
	</body>
</html>
