<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends MY_Controller
{
    function __construct()
    {
        parent::__construct(__FILE__);
        // Load any models, libraries etc. you need here
        $this->load->driver('user');
        $this->load->library('session');
    }

    /**
     * OPTIONAL; Anything in this function will be run before each test
     * Good for doing cleanup: resetting sessions, renewing objects, etc.
     */
    function _pre()
    {
        $this->session->set_userdata('account_type', MASTER);
        $this->session->set_userdata('logged_in', TRUE);
    }

    /**
    * OPTIONAL; Anything in this function will be run after each test
    * I use it for setting $this->message = $this->My_model->getError();
    */
    function _post()
    {
        $this->session->sess_destroy();
    }

    /* TESTS BELOW */

    function test_get_all_rooms()
    {
        $rooms = $this->user->get_all_rooms();

        if ( ! empty($rooms)) $rooms = TRUE;
        $this->_assert_true($rooms);
    }

    function test_get_every_room_actuators()
    {
        $rooms = $this->user->get_all_rooms();

        foreach($rooms as $room):
            $room_id = $room->id;
            $actuators = $this->user->get_actuators_by_type($room_id);

            if ( ! empty($actuators)) $actuators = TRUE;
            $this->_assert_true($actuators);

        endforeach;
    }

    function test_get_every_room_presets()
    {
        $rooms = $this->user->get_all_rooms();

        foreach($rooms as $room):
            $room_id = $room->id;
            $presets = $this->user->get_presets_by_room($room_id);

            if ( ! empty($presets)) $presets = TRUE;
            $this->_assert_true($presets);
        endforeach;

        $this->message = 'Fails if there is no preset exist in any room';
    }

    function test_get_every_room_automation()
    {
        $rooms = $this->user->get_all_rooms();

        foreach($rooms as $room):
            $room_id = $room->id;
            $automations = $this->user->get_automations_by_room($room_id);

            if ( ! empty($automations)) $automations = TRUE;
            $this->_assert_true($automations);
        endforeach;

        $this->message = 'Fails if there is no automation exist in any room';
    }

    function test_get_every_room_schedule()
    {
        $rooms = $this->user->get_all_rooms();

        foreach($rooms as $room):
            $room_id = $room->id;
            $schedules = $this->user->get_schedules_by_room($room_id);

            if ( ! empty($schedules)) $schedules = TRUE;
            $this->_assert_true($schedules);
        endforeach;

        $this->message = 'Fails if there is no schedule task exist in any room';
    }

}

// End of file example_test.php */
// Location: ./system/application/controllers/test/example_test.php */
