<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Ajax Controller
 *
 * This class handles all the AJAX requests
 * from the web application. If it's not an
 * AJAX request, it will show a 403 error by default
 * This class allows control on the Master User level access.
 *
 * @package		SmartHome
 * @category	Controller
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Ajax extends CI_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->load->library('session');

        if ( ! $this->session->userdata('logged_in')):
            show_error($this->lang->line('error_not_logged_in'), 403);
        endif;

        if ( ! $this->input->is_ajax_request()):
            show_error($this->lang->line('error_not_allowed'), 403);
        endif;

        $this->load->driver('user');
    }

    /**
     * Main function of the ajax controller
     * returns a header status 200 OK
     * for testing purposes
     */
    public function index()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->output->set_status_header('200');
    }

    /**
     * Rename room by id
     * Output HTTP Header 203 if success.
     * HTTP Header 400 if fail.
     *
     * @param int room id
     */
    public function rename_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $name = $this->input->post('name');

        if (empty($room_id) OR empty($name)):
            show_error($this->lang->line('error_uri_request'), 400);
        endif;

        if ($this->user->rename_room($room_id, $name)):
            $uri = $this->input->post('uri');
            delete_cache($uri);
            $this->output->set_status_header('203');
        else:
            show_error($this->lang->line('error_uri_request', 400));
        endif;
    }

    /**
     * Renames the actuators by id.
     * Output HTTP Header 203 if success.
     * HTTP Header 400 if fail.
     *
     * @param int actuator id
     */
    public function rename_actuator($actuator_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $name = $this->input->post('name');

        if (empty($actuator_id) OR empty($name)):
            show_error($this->lang->line('error_uri_request'), 400);
        endif;

        if ($this->user->rename_actuator($actuator_id, $name)):
            $uri = $this->input->post('uri');
            delete_cache($uri);
            $this->output->set_status_header('203');
        else:
            show_error($this->lang->line('error_uri_request', 400));
        endif;
    }

    /**
     * Calls the User driver to delete preset
     * Output HTTP Header 203 if successful
     * HTTP Header 400 if fail.
     *
     * @param int preset id
     */
    public function delete_preset($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($preset_id)):
            show_error($this->lang->line('error_uri_request' , 400));
        endif;

        if ($this->user->delete_preset($preset_id)):
            $uri = $this->input->post('uri');
            delete_cache($uri);
            $this->output->set_status_header('203');
        else:
            show_error($this->lang->line('error_uri_request' , 400));
        endif;
    }

    /**
     * Calls the User driver to delete preset
     * Output HTTP Header 203 if successful
     * HTTP Header 400 if fail.
     *
     * @param int preset id
     */
    public function delete_automation($automation_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($automation_id)):
            show_error($this->lang->line('error_uri_request' , 400));
        endif;

        if ($this->user->delete_automation($automation_id)):
            $uri = $this->input->post('uri');
            delete_cache($uri);
            $this->output->set_status_header('203');
        else:
            show_error($this->lang->line('error_uri_request' , 400));
        endif;
    }

    /**
     * This function sends the command to the actuators to perform an action
     * requested by user on the web server. Output HTTP Header 203 if successful
     * HTTP Header 400 otherwise.
     *
     * @param int actuator id
     */
    public function send_command($id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id)):
            show_error($this->lang->line('error_uri_request', 400));
        endif;

        // always send command to Actuators.
        $request_type = 'A';
        $value = $this->input->post('value');

        $result = $this->user->send_command($request_type, $id, $value);

        // result either returns TRUE/FALSE
        $result = strcmp(strtoupper($result), 'DONE') === 0 ? TRUE : FALSE;

        if ($result === TRUE):
            $this->output->set_status_header('203');
        else:
            show_error($this->lang->line('error_uri_request', 400));
        endif;
    }

    /**
     * Calls the User driver to delete preset
     * Output HTTP Header 203 if successful
     * HTTP Header 400 if fail.
     *
     * @param int schedule id
     */
    public function delete_schedule($schedule_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($schedule_id)):
            show_error($this->lang->line('error_uri_request' , 400));
        endif;

        $this->load->library('cron');
        if ($this->user->delete_schedule($schedule_id) && $this->cron->delete_schedule($schedule_id)):
            $uri = $this->input->post('uri');
            delete_cache($uri);
            $this->output->set_status_header('203');
        else:
            show_error($this->lang->line('error_uri_request' , 400));
        endif;
    }

    /**
     * Updates the status of automation to be enabled or disabled
     * Output HTTP Header 203 if successful
     * HTTP Header 400 if fail.
     *
     * @param int automation id
     */
    public function toggle_automation($automation_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($automation_id)):
            show_error($this->lang->line('error_uri_request' , 400));
        endif;

        $enabled = $this->input->post('enabled');

        if ($this->user->update_automation_status($automation_id, $enabled)):
            $uri = $this->input->post('uri');
            delete_cache($uri);
            $this->output->set_status_header('203');

        else:
            show_error($this->lang->line('error_uri_request' , 400));
        endif;
    }

    /**
     * Updates the status of schedule to be enabled or disabled
     * Output HTTP Header 203 if successful
     * HTTP Header 400 if fail.
     *
     * @param int schedule_task id
     */
    public function toggle_schedule_task($schedule_task_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($schedule_task_id)):
            show_error($this->lang->line('error_uri_request' , 400));
        endif;

        $enabled = $this->input->post('enabled');

        if ($this->user->update_schedule_status($schedule_task_id, $enabled)):
            $this->load->library('cron');
            $this->cron->toggle_schedule($schedule_task_id, $enabled);
            $uri = $this->input->post('uri');
            delete_cache($uri);
            $this->output->set_status_header('203');

        else:
            show_error($this->lang->line('error_uri_request' , 400));
        endif;
    }
}
/* End of file ajax.php */
/* Location ./app/SmartHome/controllers/ajax.php */
