<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Command Line Interface Controller
 *
 * This class handles all the CLI requests from
 * the cron jobs. If it's not a CLI request it
 * will show a 403 error by default.
 *
 * This class has direct access to the database model.
 * Use with caution!
 *
 * @package		SmartHome
 * @category	Controller
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Cli extends CI_Controller {

    private $_log_path;

    /**
     * Constructor checks if request is from CLI or not.
     */
    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if ( ! $this->input->is_cli_request()):
            show_error($this->lang->line('error_no_permission'), 403);
        endif;

        $this->load->helper('file');
    }

    /**
     * Main function of the cli controller
     * Echoes a message back to show that it works
     */
    public function index()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        echo ('CLI is working.\n');
    }

    /**
     * Remove log files which are later than specified string
     *
     * This function is called by a cron job everyday at specified time.
     *
     * @param str English date format
     */
    public function clean_logs($time = "-30 days")
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->_log_path = ($this->config->item('log_path') != '') ? $this->config->item('log_path') : APPPATH.'logs/';

        if ( ! is_dir($this->_log_path) OR ! is_really_writable($this->_log_path)):
            print ("ERROR ! " . $this->lang->line('error_no_rw_permission') . "\n");
        endif;

        $dir = get_dir_file_info($this->_log_path);

        foreach($dir as $file => $info):
            if (strcmp($file, 'index.html') === 0) continue;

            $date = substr($file, 4, -4);

            if (strtotime($date) < strtotime($time)):
                echo "{$file} will be deleted. \n";

                unlink($info['server_path']);
            endif;
        endforeach;
    }

    /**
     * Clear all caches
     *
     * This function clears all caches.
     */
    public function clear_all_cache()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->_cache_path = ($this->config->item('cache_path') != '') ? $this->config->item('cache_path') : APPPATH.'cache/';

        if ( ! is_dir($this->_cache_path) OR ! is_really_writable($this->_cache_path)):
            print ("ERROR ! " . $this->lang->line('error_no_rw_permission') . "\n");
        endif;

        $dir = get_dir_file_info($this->_cache_path);

        foreach($dir as $file => $info):
            if (strcmp($file, 'index.html') === 0) continue;

            echo "{$file} will be deleted. \n";
            unlink($info['server_path']);
        endforeach;
    }

    /**
     * Execute preset!
     *
     * @param int preset id
     */
    public function run_preset($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($preset_id)):
            log_message('error', 'Preset ID not specified');
        endif;

        $this->load->model('preset');

        $state = $this->preset->get_state($preset_id);

        foreach($state as $id => $data):
            if ($data['enabled'] == 1):
                echo "Actuator ID: {$id} \nValue: {$data['value']}\n";
                $this->socket->send('A', $id, $data['value']);
            endif;
        endforeach;
    }
}
/* End of file cli.php */
/* Location ./app/SmartHome/controllers/cli.php */

