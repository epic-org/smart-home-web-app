<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Smarty_pants Controller
 *
 * This class handles site wide page routing.
 * It also handles site sessions.
 *
 * @package		SmartHome
 * @category	Controller
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Smarty_pants extends CI_Controller {

    private $data = array(
        'href' => array(
            'logout' => 'logout',
            'rooms' =>  'smarty_pants/rooms',
            'devices' => 'smarty_pants/controls',
            'presets' => 'smarty_pants/presets',
            'edit_preset' => 'smarty_pants/edit_preset',
            'new_preset' => 'smarty_pants/new_preset',
            'auto' => 'smarty_pants/automation',
            'new_auto' => 'smarty_pants/new_auto',
            'edit_auto' => 'smarty_pants/edit_auto',
            'new_schedule' => 'smarty_pants/new_schedule',
            'schedule' => 'smarty_pants/schedule',
            'edit_schedule' => 'smarty_pants/edit_schedule'
        )
    );


    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->load->library('session');

        // Verify login status
        if ( ! $this->session->userdata('logged_in')) redirect('/login');

        // Show profiler depending on environment
        $this->output->enable_profiler($this->config->item('profiling'));

        $this->load->driver('user');

        $this->_load_links();
        $this->_load_templates();
    }

    /**
     * Index page loads the list of rooms
     */
    public function index()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->rooms();
    }

    /**
     * Display list of rooms to user.
     * Get list of rooms from the database and populate..
     *
     * @model room
     */
    public function rooms()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->user->check_authority(GUEST);

        // Query list of rooms
        $rooms = $this->user->get_all_rooms();

        // Load view
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);
        $alerts['info_rename'] = $this->load->view('templates/global_alerts/info_rename', $this->data, TRUE);

        // Load data for view
        $this->data['rooms'] = $rooms;
        $this->data['alerts'] = $alerts;

        // Publish!
        $this->load->view('rooms', $this->data);

        // Cache for 24 hours
        $this->output->cache(ONE_DAY);
    }

    /**
     * Display list of actuators in specified room to users.
     * If no room specified, redirect to room list.
     *
     * @param int room_id
     */
    public function controls($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Do checking
        $this->user->check_authority(GUEST);
        $this->_check_room_id($room_id);

        // Load data for view
        $actuators = $this->user->get_actuators_by_type($room_id);

        // Load views
        $this->data['maintabs'] = $this->load->view('templates/maintabs', $this->data, TRUE);
        $this->data['room_name'] = $this->load->view('templates/room_name', $this->data, TRUE);
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);

        $data['alerts']['info_rename'] = $this->load->view('templates/global_alerts/info_rename', $this->data, TRUE);
        $this->data['alerts'] = $data['alerts'];

        $data['actuators'] = $actuators[LIGHT];
        $this->data['controls'][LIGHT] = $this->load->view('controls/lights', $data, TRUE);
        $data['actuators'] = $actuators[MOTOR];
        $this->data['controls'][MOTOR] = $this->load->view('controls/motors', $data, TRUE);
        $data['actuators'] = $actuators[AC];
        $this->data['controls'][AC] = $this->load->view('controls/ac', $data, TRUE);

        // publish!
        $this->load->view('controls', $this->data);

        // Cache for 24 hours
        $this->output->cache(ONE_DAY);
    }

    /**
     * Display list of presets in that room.
     * If no room specified, redirect to room list.
     *
     * @model preset
     *
     * @param int room_id
     */
    public function presets($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Do checking
        $this->user->check_authority(NORMAL);
        $this->_check_room_id($room_id);

        // Get data from model
        $presets = $this->user->get_presets_by_room($room_id);

        // Load data for view
        $this->data['presets'] = $presets;

        $alerts['info_rename'] = $this->load->view('templates/global_alerts/info_rename', '', TRUE);

        $this->data['alerts'] = $alerts;

        // Load view
        $this->data['maintabs'] = $this->load->view('templates/maintabs', $this->data, TRUE);
        $this->data['room_name'] = $this->load->view('templates/room_name', $this->data, TRUE);
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);

        // Publish!
        $this->load->view('presets', $this->data);

        // Cache for 24 hours
        $this->output->cache(ONE_DAY);
    }

    /**
     * Edit preset by it's ID
     * if preset_id is not specified, redirect to presets list.
     *
     * @model preset
     * @model preset_actuator
     * @model actuator
     *
     * @param int preset_id
     */
    public function edit_preset($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Disable caching
        $this->output->no_cache();

        // Do checking
        $this->user->check_authority(MASTER);
        if ( ! $preset_id) redirect($this->data['href']['presets']);

        $this->data['preset_id'] = $preset_id;

        $name = $this->input->post('preset_name');
        $preset_data = $this->input->post('actuators');

        // Check for submitted form
        if ($this->input->post() !== FALSE):
            $this->form_validation->set_message('is_unique', $this->lang->line('error_name_exists'));

            if ($this->form_validation->run(__FUNCTION__) != FALSE):
                // If editing preset is successful, redirect to presets page
                if ($this->user->edit_preset($preset_id, $name, $preset_data)):
                    $room_id = $this->session->userdata('room_id');
                    $uri = $this->data['href']['presets'] . '/' . $room_id;
                    // only clear cache if name has been changed
                    if ($name !== FALSE) delete_cache($uri);
                    redirect($uri);

                endif;

            else:
                // Repopulate form
                $this->data['_actuators'] = $preset_data;

            endif;
        endif;

        // Query current preset
        $preset = $this->user->get_preset($preset_id);

        // Query actuators from model
        $actuators = $this->user->get_actuators_by_type($this->session->userdata('room_id'));

        // Query actuator states
        $state = $this->user->get_preset_state($preset_id);

        // Load view
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);

        // Load data for view
        $data['actuators'] = $actuators;
        $data['state'] = $state;
        $data['actuators'] = $actuators[LIGHT];
        $data['index'] = 0;
        $data['count'] = count($actuators[LIGHT]);
        $edit_preset[LIGHT] = $this->load->view('presets/lights', $data, TRUE);

        $data['actuators'] = $actuators[MOTOR];
        $data['index'] = $data['count'];
        $data['count'] = $data['count'] + count($actuators[MOTOR]);
        $edit_preset[MOTOR] = $this->load->view('presets/motors', $data, TRUE);

        $data['actuators'] = $actuators[AC];
        $data['index'] = $data['count'];
        $data['count'] = $data['count'] + count($actuators[AC]);
        $edit_preset[AC] = $this->load->view('presets/ac', $data, TRUE);

        $alerts['green_panels'] = $this->load->view('presets/alerts/green_panels', $this->data, TRUE);

        $this->data['preset'] = $preset;
        $this->data['presets']['edit_preset'] = $edit_preset;
        $this->data['presets']['alerts'] = $alerts;

        // Publish!
        $this->load->view('edit_preset', $this->data);
    }

    /**
     * Creates new preset
     *
     * @model preset
     * @model preset_actuator
     * @model actuator
     *
     * @param int room_id
     */
    public function new_preset($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Disable caching
        $this->output->no_cache();

        // Do checking
        $this->user->check_authority(MASTER);
        $this->_check_room_id($room_id);

        $name = $this->input->post('preset_name');
        $preset_data = $this->input->post('actuators');

        // Check for submitted form
        if ($this->input->post() !== FALSE):
            $this->form_validation->set_message('required', $this->lang->line('error_empty_field'));
            $this->form_validation->set_message('is_unique', $this->lang->line('error_name_exists'));

            if ($this->form_validation->run(__FUNCTION__) != FALSE):
                // if adding new preset is successful, redirect to presets page
                if ($this->user->add_preset($name, $room_id, $preset_data)):
                    $uri = $this->data['href']['presets'] . '/' . $room_id;
                    delete_cache($uri);
                    redirect($uri);

                endif;
            else:
                // To repopulate the form
                $this->data['_actuators'] = $preset_data;

            endif;
        endif;

        // Query actuators from model
        $actuators = $this->user->get_actuators_by_type($room_id);

        // Load view
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);

        $data['actuators'] = $actuators[LIGHT];
        $data['index'] = 0;
        $data['count'] = count($actuators[LIGHT]);
        $new_preset[LIGHT] = $this->load->view('presets/lights', $data, TRUE);

        $data['actuators'] = $actuators[MOTOR];
        $data['index'] = $data['count'];
        $data['count'] = $data['count'] + count($actuators[MOTOR]);
        $new_preset[MOTOR] = $this->load->view('presets/motors', $data, TRUE);

        $data['actuators'] = $actuators[AC];
        $data['index'] = $data['count'];
        $data['count'] = $data['count'] + count($actuators[AC]);
        $new_preset[AC] = $this->load->view('presets/ac', $data, TRUE);

        $alerts['green_panels'] = $this->load->view('presets/alerts/green_panels', $this->data, TRUE);

        // Load data for view
        $this->data['presets']['new_preset'] = $new_preset;
        $this->data['presets']['alerts'] = $alerts;

        // Publish
        $this->load->view('new_preset', $this->data);
    }

    /**
     * Display list of automation.
     * If no room_id specified, redirect to room list
     *
     * @model automation
     *
     * @param int room_id
     */
    public function automation($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Do checking
        $this->user->check_authority(NORMAL);
        $this->_check_room_id($room_id);

        // Query automations from database
        $automations = $this->user->get_automations_by_room($room_id);

        // Load data for view
        $this->data['automations'] = $automations;

        // Load view
        $this->data['maintabs'] = $this->load->view('templates/maintabs', $this->data, TRUE);
        $this->data['room_name'] = $this->load->view('templates/room_name', $this->data, TRUE);
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);

        // Publish!
        $this->load->view('auto', $this->data);

        // Cache for 24 hours
        $this->output->cache(ONE_DAY);
    }

    /**
     * Edit automation
     *
     * @model automation
     * @model automation_preset
     * @model automation_sensor
     * @model preset
     * @model sensor
     *
     * @param int automation_id
     */
    public function edit_auto($automation_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Disable caching
        $this->output->no_cache();

        // Do checking
        $this->user->check_authority(MASTER);
        if ( ! $automation_id) redirect($this->data['href']['auto']);

        if ($this->input->post()):
            $this->form_validation->set_message('required', $this->lang->line('error_empty_field'));
            $this->form_validation->set_message('is_unique', $this->lang->line('error_name_exists'));
            $this->form_validation->set_message('integer', $this->lang->line('error_invalid_input'));

            if($this->form_validation->run(__FUNCTION__) != FALSE):
                $name = $this->input->post('automation_name');
                $presets = $this->input->post('presets');
                $sensors = $this->input->post('sensors');
                $custom = $this->input->post('custom');

                if ($custom == FALSE):
                    $sensor_value  = $this->input->post('sensor_value');

                    if ($sensor_value{0} == '<') $sensors['more_than'] = 0;

                    elseif ($sensor_value{0} == '>') $sensors['more_than'] = 1;

                    $sensors['sensor_value'] = substr($sensor_value, 1);

                else:
                    $sensors['more_than'] = $this->input->post('more_than');
                    $sensors['sensor_value'] = $this->input->post('sensor_custom_value');

                endif;

                if ($this->user->edit_automation($automation_id, $name, $presets, array($sensors))):
                    $room_id = $this->session->userdata('room_id');
                    $uri = $this->data['href']['auto'] . '/' . $room_id;

                    if ($name !== FALSE) delete_cache($uri);

                    redirect($uri);

                endif;
            endif;
        endif;

        // Query for view
        $presets = $this->user->get_presets_by_room($this->session->userdata('room_id'));
        $sensors = $this->user->get_sensors_by_room($this->session->userdata('room_id'));

        $this->data['presets'] = $presets;
        $this->data['sensors'] = $sensors;

        $automation_state = $this->user->get_automation_state($automation_id);

        if ( ! empty($automation_state)):
            $sensor_value = $this->user->get_sensor_reading('S', $automation_state->sensor_id);

            $this->data['automation_state'] = $automation_state;

            $this->data['sensor_value'] =  $sensor_value;
            $this->data['init_sensor_id'] = $automation_state->sensor_id;
            $this->data['init_max_value'] = $automation_state->max_value;
            $this->data['val'] = (int) ($sensor_value / $automation_state->max_value * 100);

        else:
            log_message('error', $this->lang->line('error_no_auto_state'));
            $sensor_value = $this->user->get_sensor_reading('S', $sensors[0]->id);
            $this->data['sensor_value'] = $sensor_value;
            $this->data['init_sensor_id'] = $sensors[0]->id;
            $this->data['init_max_value'] = $sensors[0]->max_value;
            $this->data['val'] = (int) ($sensor_value / $sensors[0]->max_value * 100);

        endif;

        // Load view
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);
        $this->data['auto']['step_1'] = $this->load->view('auto/step_1', $this->data, TRUE);
        $this->data['auto']['step_2'] = $this->load->view('auto/step_2', $this->data, TRUE);
        $this->data['auto']['step_3'] = $this->load->view('auto/step_3', $this->data, TRUE);
        $this->data['auto']['step_4'] = $this->load->view('auto/step_4', $this->data, TRUE);
        $this->data['auto']['step_5'] = $this->load->view('auto/step_5', $this->data, TRUE);

        // Publish!
        $this->load->view('edit_auto', $this->data);
    }

    /**
     * New automation
     *
     * @model automation
     * @model automation_preset
     * @model automation_sensor
     * @model preset
     * @model sensor
     *
     * @param int room id
     */
    public function new_auto($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Disable caching
        $this->output->no_cache();

        // Do checking
        $this->user->check_authority(MASTER);
        $this->_check_room_id($room_id);

        if ($this->input->post()):
            $this->form_validation->set_message('required', $this->lang->line('error_empty_field'));
            $this->form_validation->set_message('is_unique', $this->lang->line('error_name_exists'));
            $this->form_validation->set_message('integer', $this->lang->line('error_invalid_input'));

            if ($this->form_validation->run(__FUNCTION__) != FALSE):
                $name = $this->input->post('automation_name');
                $presets = $this->input->post('presets');
                $sensors = $this->input->post('sensors');
                $custom = $this->input->post('custom');

                // set value depending on custom choice (custom or otherwise)
                if ($custom == FALSE):
                    $sensor_value  = $this->input->post('sensor_value');

                    if ($sensor_value{0} == '<') $sensors['more_than'] = 0;

                    elseif ($sensor_value{0} == '>') $sensors['more_than'] = 1;

                    $sensors['sensor_value'] = substr($sensor_value, 1);

                else:
                    $sensors['more_than'] = $this->input->post('more_than');
                    $sensors['sensor_value'] = $this->input->post('sensor_custom_value');

                endif;

                // Add new automation to db
                if ($this->user->add_automation($name, $room_id, $presets, array($sensors))):
                    $uri = $this->data['href']['auto'] . '/' . $room_id;
                    delete_cache($uri);
                    redirect($uri);

                endif;
            endif;
        endif;

        // Query data from db
        $presets = $this->user->get_presets_by_room($this->session->userdata('room_id'));

        // No preset found, ask user to create a new preset.
        if (empty($presets)):
            $this->session->set_flashdata('error', $this->lang->line('error_no_preset'));
            delete_cache($this->data['href']['presets'].'/'.$this->session->userdata('room_id'));
            redirect($this->data['href']['presets'].'/'.$this->session->userdata('room_id'));

        endif;

        $sensors = $this->user->get_sensors_by_room($this->session->userdata('room_id'));
        $sensor_value = $this->user->get_sensor_reading('S', $sensors[0]->id);

        // Load data for view
        $this->data['presets'] = $presets;
        $this->data['sensors'] = $sensors;
        $this->data['sensor_value'] = $sensor_value;
        $this->data['init_sensor_id'] = $sensors[0]->id;
        $this->data['init_max_value'] = $sensors[0]->max_value;
        $this->data['val'] = (int) ($sensor_value / $sensors[0]->max_value * 100);

        // Load view
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);
        $this->data['auto']['step_1'] = $this->load->view('auto/step_1', $this->data, TRUE);
        $this->data['auto']['step_2'] = $this->load->view('auto/step_2', $this->data, TRUE);
        $this->data['auto']['step_3'] = $this->load->view('auto/step_3', $this->data, TRUE);
        $this->data['auto']['step_4'] = $this->load->view('auto/step_4', $this->data, TRUE);
        $this->data['auto']['step_5'] = $this->load->view('auto/step_5', $this->data, TRUE);

        // Publish!
        $this->load->view('new_auto', $this->data);
    }

    /**
     * Scheduling page
     *
     * @model schedule_task
     *
     * @param int room id
     */
    public function schedule($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Do checking
        $this->user->check_authority(NORMAL);
        $this->_check_room_id($room_id);

        // Load helper for converting display on view
        $this->load->helper('date');
        $this->config->load('format', TRUE);

        // Query schedules from database
        $schedules = $this->user->get_schedules_by_room($room_id);

        // Load data for view
        $this->data['schedules'] = $schedules;

        // Load view
        $this->data['maintabs'] = $this->load->view('templates/maintabs', $this->data, TRUE);
        $this->data['room_name'] = $this->load->view('templates/room_name', $this->data, TRUE);
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);

        // Publish!
        $this->load->view('schedule', $this->data);

        // Cache for 24 hours
        $this->output->cache(ONE_DAY);
    }

    /**
     * Edit scheduling page
     *
     * @model schedule_task
     * @model scheduled_preset
     * @model preset
     *
     * @param int schedule_id
     */
    public function edit_schedule($schedule_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Disable caching
        $this->output->no_cache();

        // Do checking
        $this->user->check_authority(MASTER);
        if ( ! $schedule_id) redirect($this->data['href']['schedule']);

        // Load helper for converting display on view
        $this->load->helper('date');
        $this->config->load('format', TRUE);
        $format = $this->config->item('format');

        if ($this->input->post()):
            $this->form_validation->set_message('required', $this->lang->line('error_empty_field'));
            $this->form_validation->set_message('is_unique', $this->lang->line('error_name_exists'));
            $this->form_validation->set_message('integer', $this->lang->line('error_invalid_input'));

            if ($this->form_validation->run(__FUNCTION__) != FALSE):
                $schedule = $this->input->post('schedule');
                $preset_id = $this->input->post('preset_id');

                // Format time to be compatible with sql db
                $time = DateTime::createFromFormat('g:i A', $schedule['time']);
                $schedule['time'] = $time->format($format['time']);

                if ((int) $schedule['day'] === 0 AND ! empty($schedule['date'])):
                    // Format date to be compatible with sql db
                    $date = DateTime::createFromFormat('d/m/Y', $schedule['date']);
                    $schedule['date'] = $date->format($format['date']);

                // If repeat is chosen, remove date.
                elseif ((int) $schedule['day'] > 0): $schedule['date'] = NULL;

                endif;

                if (empty($schedule['name'])) $schedule['name'] = $this->input->post('placeholder');

                $room_id = $this->session->userdata('room_id');

                if ($this->user->edit_schedule($schedule_id, $schedule, $preset_id, $room_id)):
                    $this->load->library('cron');

                    if ($this->cron->edit_schedule($schedule, $preset_id, $schedule_id, $format['date'], $format['time'])):
                        $uri = $this->data['href']['schedule'] . '/' . $room_id;
                        delete_cache($uri);
                        redirect($uri);

                    endif;
                endif;
            endif;
        endif;

        // Query data from db
        $presets = $this->user->get_presets_by_room($this->session->userdata('room_id'));
        $schedule_state = $this->user->get_schedule_state($schedule_id);

        // Prepare data for view
        $this->data['presets'] = $presets;
        $this->data['schedule_state'] = $schedule_state;

        // Load views
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);
        $this->data['schedule']['name'] =  $this->load->view('schedule/name', $this->data, TRUE);
        $this->data['schedule']['time_date'] = $this->load->view('schedule/time_date', $this->data, TRUE);
        $this->data['schedule']['repeat'] = $this->load->view('schedule/repeat', $this->data, TRUE);
        $this->data['schedule']['selection'] = $this->load->view('schedule/selection', $this->data, TRUE);

        // Publish!
        $this->load->view('edit_schedule', $this->data);
    }

    /**
     * New schedule
     *
     * @model schedule_task
     * @model scheduled_preset
     * @model preset
     *
     * @param int room_id
     */
    public function new_schedule($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // Disable caching
        $this->output->no_cache();

        // Do checking
        $this->user->check_authority(MASTER);
        $this->_check_room_id($room_id);

        // Load config for switching date
        $this->config->load('format', TRUE);
        $format = $this->config->item('format');

        if ($this->input->post()):
            $this->form_validation->set_message('required', $this->lang->line('error_empty_field'));
            $this->form_validation->set_message('is_unique', $this->lang->line('error_name_exists'));
            $this->form_validation->set_message('integer', $this->lang->line('error_invalid_input'));

            if ($this->form_validation->run(__FUNCTION__) != FALSE):
                $schedule = $this->input->post('schedule');
                $preset_id = $this->input->post('preset_id');

                // Format time to be compatible with sql db
                $time = DateTime::createFromFormat('g:i A', $schedule['time']);
                $schedule['time'] = $time->format($format['time']);

                if ((int) $schedule['day'] === 0 AND ! empty($schedule['date'])):
                    // Format date to be compatible with sql db
                    $date = DateTime::createFromFormat('d/m/Y', $schedule['date']);
                    $schedule['date'] = $date->format($format['date']);

                // If repeat is chosen, remove date.
                elseif ((int) $schedule['day'] > 0): unset($schedule['date']);

                endif;

                // Add new schedule to database
                if ($schedule_id = $this->user->add_schedule($schedule, $preset_id, $room_id)):
                    $this->load->library('cron');

                    // Add new cronjob to execute preset
                    if ($this->cron->add_schedule($schedule, $preset_id, $schedule_id, $format['date'], $format['time'])):
                        $uri = $this->data['href']['schedule'] . '/' . $room_id;
                        delete_cache($uri);
                        redirect($uri);

                    endif;
                endif;
            endif;
        endif;

        // Query data from db
        $presets = $this->user->get_presets_by_room($room_id);

        // No preset found, ask user to create a new preset.
        if (empty($presets)):
            $this->session->set_flashdata('error', $this->lang->line('error_no_preset'));
            delete_cache($this->data['href']['presets'].'/'.$this->session->userdata('room_id'));
            redirect($this->data['href']['presets'].'/'.$this->session->userdata('room_id'));

        endif;

        // Prepare data for view
        $this->data['presets'] = $presets;

        // Load views
        $this->data['hidden'] = $this->load->view('templates/hidden', $this->data, TRUE);
        $this->data['schedule']['name'] =  $this->load->view('schedule/name', $this->data, TRUE);
        $this->data['schedule']['time_date'] = $this->load->view('schedule/time_date', $this->data, TRUE);
        $this->data['schedule']['repeat'] = $this->load->view('schedule/repeat', $this->data, TRUE);
        $this->data['schedule']['selection'] = $this->load->view('schedule/selection', $this->data, TRUE);

        // Publish!
        $this->load->view('new_schedule', $this->data);
    }

    /**
     * Loads the settings view
     *
     * @return str HTML view for settings modal
     */
    private function _load_account_settings()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $data = array();

        $data['change_pin']['master'] = $this->load->view('templates/settings/change_pin/master', '', TRUE);
        $data['change_pin']['normal'] = $this->load->view('templates/settings/change_pin/normal', '', TRUE);
        $data['change_password']['master'] = $this->load->view('templates/settings/change_password/master', '', TRUE);
        $data['change_password']['normal'] = $this->load->view('templates/settings/change_password/normal', '', TRUE);
        // Add new schedule to database

        $change_pin = $this->load->view('templates/settings/change_pin', $data, TRUE);
        $change_password = $this->load->view('templates/settings/change_password', $data, TRUE);
        $change_email = $this->load->view('templates/settings/change_email', '', TRUE);

        $this->data['settings']['change_pin'] = $change_pin;
        $this->data['settings']['change_password'] = $change_password;
        $this->data['settings']['change_email'] = $change_email;

        $settings = $this->load->view('templates/settings' , $this->data, TRUE);

        return $settings;
    }

    /**
     * Check room existence.
     *
     * Eventually, set the session variables 'room_id'
     *
     * @param int room id
     */
    private function _check_room_id($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($room_id)):
            // room_id is empty
            // Redirect user with flashdata
            $this->session->set_flashdata('error', $this->lang->line('error_no_room_specified'));
            delete_cache($this->data['href']['rooms']);
            redirect($this->data['href']['rooms']);

        elseif ( ! empty($room_id)):
            $room = $this->user->get_room($room_id);

            // Query returns false
            if ($room === NULL):
                $this->session->set_flashdata('error', $this->lang->line('error_no_room_specified'));
                delete_cache($this->data['href']['rooms']);
                redirect($this->data['href']['rooms']);

            endif;

            $this->data['room_id'] = $room->id;
            $this->data['room_name'] = $room->name;
            $this->session->set_userdata('room_id', $room->id);

        endif;
    }

    /**
     * Called by the constructor, this function pre-loads all links that
     * shared by almost all of the pages.
     */
    private function _load_links()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->data['ajax']['rename_room'] = site_url('ajax/rename_room');
        $this->data['ajax']['rename_actuator'] = site_url('ajax/rename_actuator');
        $this->data['ajax']['send_command'] = site_url('ajax/send_command');
        $this->data['ajax']['delete_preset'] = site_url('ajax/delete_preset');
        $this->data['ajax']['delete_automation'] = site_url('ajax/delete_automation');
        $this->data['ajax']['toggle_automation'] = site_url('ajax/toggle_automation');
        $this->data['ajax']['delete_schedule'] = site_url('ajax/delete_schedule');
        $this->data['ajax']['toggle_schedule_task'] = site_url('ajax/toggle_schedule_task');

        $this->data['json']['room_name'] = site_url('json/get_room_name');
        $this->data['json']['get_preset_state'] = site_url('json/get_preset_state');
        $this->data['json']['sensor_reading'] = site_url('json/get_sensor_reading');
        $this->data['json']['user'] = site_url('json/get_user_data');
        $this->data['json']['update_settings'] = site_url('json/update_settings');
    }

    /**
     * Loads templates that are used by all pages
     */
    private function _load_templates()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->data['head'] = $this->load->view('templates/head', '', TRUE);
        $this->data['js'] = $this->load->view('templates/js', '', TRUE);
        $this->data['settings'] = $this->_load_account_settings();
        $this->data['topstrip'] = $this->load->view('templates/topstrip', $this->data, TRUE);
    }
}

/* End of file Smarty_pants.php */
/* Location: ./app/SmartHome/controllers/smarty_pants.php */

