<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Login Controller
 *
 * This class handles all login request be it
 * remotely or locally. User are redirected
 * based on their IP address.
 *
 * @package		SmartHome
 * @category	Controller
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Login extends CI_Controller {

    private $data = array();

    private $site = 'smarty_pants';

    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->load->library('session');
        $this->load->driver('user');
        $this->load->helper('security');

        // Show profiler depending on environment
        $this->output->enable_profiler($this->config->item('profiling'));

        $this->session->set_userdata('private_lan', $this->_private_network());

        // If user is logged in, redirect to default controller
        if ($this->session->userdata('logged_in')) redirect($this->site);

        $this->output->no_cache();
    }

    /**
     * Show login page and load it according to their IP range.
     */
    public function index()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->data['head'] = $this->load->view('templates/head', '', TRUE);
        $this->data['alerts']['login'] = $this->load->view('templates/global_alerts/danger_enable', '', TRUE);
        $this->data['js'] = $this->load->view('templates/js', '', TRUE);

        if ($this->input->post('guest')) $this->_login_guest();

        // Loads different login page based on their IP address
        if ($this->session->userdata('private_lan') === TRUE):
            if($this->input->post('forgot_pin')) $this->_forgot_pin();
            if ($this->input->post()) $this->_login_pin();
            $this->load->view('login_local', $this->data);

        else:
            if($this->input->post('forgot_password')) $this->_forgot_password();
            if ($this->input->post()) $this->_login_password();
            $this->load->view('login_remote', $this->data);

        endif;
    }

    /**
     * Handles login submission from remote host using password
     *
     * If login successful, redirect to main page
     * Else, refresh and show error
     */
    private function _login_password()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->form_validation->set_message('required', $this->lang->line('error_empty_field'));
        $this->form_validation->set_message('integer', $this->lang->line('error_invalid_input'));
        if ($this->form_validation->run(__FUNCTION__) == FALSE): return;

        else:

            $account_type = $this->input->post('account_type');
            $password = do_hash($this->input->post('password'));

            // If authentication successful, set session logged_in and account_type
            if ($this->user->authenticate_user_password($account_type, $password)):
                $this->session->set_userdata('account_type', $account_type);
                $this->session->set_userdata('logged_in', TRUE);
                $this->input->set_cookie('permission', $account_type, 0);
                redirect($this->site);

            else:
                $this->session->set_flashdata('error', $this->lang->line('error_login_password'));
                redirect('/login');

            endif;
        endif;
    }

    /**
     * Handles login submission from local host using PIN
     *
     * If login successful, redirect to main page
     * Else, refresh and show error
     */
    private function _login_pin()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->form_validation->set_message('required', $this->lang->line('error_empty_field'));
        $this->form_validation->set_message('exact_length', $this->lang->line('error_pin_length'));

        if ($this->form_validation->run(__FUNCTION__) == FALSE): return;

        else:
            $pin = do_hash($this->input->post('pin'));

            // If authentication successful, get account_type and set session variables
            if ($account_type = $this->user->authenticate_user_pin($pin)):
                $this->session->set_userdata('account_type', $account_type);
                $this->session->set_userdata('logged_in', TRUE);

                $this->input->set_cookie('permission', $account_type, 0);
                redirect($this->site);

            else:
                $this->session->set_flashdata('error', $this->lang->line('error_empty_field'));
                redirect('/login');

            endif;
        endif;
    }

    /**
     * Login user as guest
     */
    private function _login_guest()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->session->set_userdata('account_type', GUEST);
        $this->session->set_userdata('logged_in', TRUE);
        $this->input->set_cookie('permission', GUEST, 0);
        redirect($this->site);
    }

    /**
     * Check user network type (local / remote)
     *
     * @return boolean  TRUE means local host, FALSE means remote host
     */
    private function _private_network()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());
        // Convert IP to integer to check if it's in private LAN
        // @link http://stackoverflow.com/a/7614263
        $ip = $this->input->ip_address();
        $lan = $this->config->item('ips');

        $ip = ip2long($ip);

        $private_network = FALSE;

        foreach ($lan as $private_lan):
            if ($ip >= $private_lan['start'] && $ip <= $private_lan['end']):
                // IP is in range
                $private_network = TRUE;

            endif;
        endforeach;

        return $private_network;
    }

    private function _forgot_pin()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $email = $this->user->get_master_email($this->input->post('email'));

        if ( ! empty($email)):
            $this->load->helper('string');
            $this->load->library('email');
            $rand = random_string('numeric', 4);
            $this->email->set_newline("\r\n");
            $this->email->to($email);
            $this->email->from('EPICL.org@gmail.com', 'Maxtrack Support');
            $this->email->subject('Forgot Pin Code');
            $this->email->message("Your new pin is {$rand}");
            $result = $this->email->send();

            if ($result == 1):
                $this->user->update_master_pin(do_hash($rand));

            else:
                $this->session->set_flashdata('error', $this->lang->line('error_fail_to_send_email'));
                redirect('login');

            endif;

        else:
            $this->session->set_flashdata('error', $this->lang->line('error_wrong_email'));
            redirect('login');

        endif;
    }

    private function _forgot_password()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $email = $this->user->get_master_email($this->input->post('email'));

        if ( ! empty($email)):
            $this->load->helper('string');
            $this->load->library('email');
            $rand = random_string('alpha', 8);
            $this->email->set_newline("\r\n");
            $this->email->to($email);
            $this->email->from('EPICL.org@gmail.com', 'Maxtrack Support');
            $this->email->subject('Forgot Password');
            $this->email->message("Your new password is {$rand}");
            $result = $this->email->send();

            if ($result == 1):
                $this->user->update_master_password(do_hash($rand));

            else:
                $this->session->set_flashdata('error', $this->lang->line('error_fail_to_send_email'));
                redirect('login');

            endif;

        else:
            $this->session->set_flashdata('error', $this->lang->line('error_wrong_email'));
            redirect('login');

        endif;
    }
}

/* End of file login.php */
/* Location: ./app/SmartHome/controllers/login.php */

