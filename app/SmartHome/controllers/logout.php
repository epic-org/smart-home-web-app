<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Logout Controller
 *
 * Just logs out the user. A light controller
 * that doesn't preload anything unnecessary.
 *
 * @package		SmartHome
 * @category	Controller
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Logout extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->load->library('session');

        $this->session->sess_destroy();

        $this->input->set_cookie('permission', 0, 0);

        redirect('/login');
    }
}

/* End of file logout.php */
/* Location: ./app/SmartHome/controllers/logout.php */

