<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome JSON Controller
 *
 * This class is the API for all JSON data that can only get by
 * AJAX request. It will show a 403 error by default if not AJAX.
 * This class always return a JSON type on success.
 *
 * @package		SmartHome
 * @category	Controller
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Json extends CI_Controller {

    private $data = array();

    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->load->library('session');

        if ( ! $this->session->userdata('logged_in')):
            show_error($this->lang->line('error_not_logged_in'), 403);
        endif;

        if ( ! $this->input->is_ajax_request()):
            show_error($this->lang->line('error_not_allowed'), 403);
        endif;

        $this->load->driver('user');
    }

    /**
     * Main function of the JSON API
     * returns a header status 200 OK
     * for testing purposes.
     */
    public function index()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->output->set_status_header('200');
        $this->output->set_content_type('application/json');
        $this->set_output(json_encode(array('it' => 'works!')));
    }

    /**
     * This function gets the sensor reading from the sensor.
     * Outputs JSON array('value' => $value)
     * If fail outputs HTTP Header 400.
     *
     * @param int sensor id
     */
    public function get_sensor_reading($id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id)):
            show_error($this->lang->line('error_uri_request', 400));
        endif;

        // always send commmand to Sensor
        $request_type = 'S';

        $result = '';
        $result = $this->user->get_sensor_reading($request_type, $id);

        if ( ! empty($result)):
            $this->output->set_content_type('application/json')
                ->set_output(json_encode(array('value' => $result)));
        else:
            show_error($this->lang->line('error_uri_request', 400));
        endif;
    }

    /**
     * Gets the room name
     *
     * @param int room id
     */
    public function get_room_name($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $room_id = empty($room_id) ? $this->session->userdata('room_id') : $room_id;

        if (empty($room_id)):
            show_error($this->lang->line('error_uri_request', 400));
        endif;

        $name =  $this->user->get_room_name($room_id);

        if ( ! empty($name)):
            $this->output->set_content_type('application/json')
                ->set_output(json_encode($name));
        else:
            show_error($this->lang->line('error_uri_request', 400));
        endif;
    }

    /**
     * Get user status
     */
    public function get_user_data()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $user['logged_in'] = $this->session->userdata('logged_in');
        $type = $this->session->userdata('account_type');

        $user['type'] = $type == GUEST ? 'GUEST' : ($type == NORMAL ? 'NORMAL' : ($type == MASTER ? 'MASTER' : -1));

        if ($user['type'] != -1):
            $this->output->set_content_type('application/json')
                ->set_output(json_encode($user));
        else:
            show_error($this->lang->line('error_no_user_found'), 400);
        endif;
    }

    /**
     * Get preset state for command execution
     * HTTP Header 400 if fail
     *
     * @param int preset id
     */
    public function get_preset_state($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($preset_id)):
            show_error($this->lang->line('error_uri_request' , 400));
        endif;

        $state =  $this->user->get_preset_state($preset_id);

        if ( ! empty($state)):
            $this->output->set_content_type('application/json')
                ->set_output(json_encode($state));
        else:
            show_error($this->lang->line('error_uri_request' , 400));
        endif;
    }

    /**
     * Update account settings
     * Output JSON format. The new setting's status.
     */
    public function update_settings()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->load->helper('security');

        $state = array();

        $master_pin = $this->input->post('master_pin');
        $normal_pin = $this->input->post('normal_pin');
        $master_password = $this->input->post('master_password');
        $normal_password = $this->input->post('normal_password');
        $master_email = $this->input->post('master_email');

        if ( ! empty($master_pin) && count($master_pin) === 3):
            foreach ($master_pin as $key => $value):
                if ($value != ''):
                    $master_pin[$key] = do_hash($value);
                endif;
            endforeach;

            if ($this->_update_master_pin($master_pin)):
                $state['master_pin'] = TRUE;
            else:
                $state['master_pin'] = FALSE;
            endif;
        endif;

        if ( ! empty($normal_pin) && count($normal_pin) === 3):
            foreach ($normal_pin as $key => $value):
                if ($value != ''):
                    $normal_pin[$key] = do_hash($value);
                endif;
            endforeach;

            if ($this->_update_normal_pin($normal_pin)):
                $state['normal_pin'] = TRUE;
            else:
                $state['normal_pin'] = FALSE;
            endif;
        endif;

        if ( ! empty($master_password) && count($master_password) === 3):
            foreach ($master_password as $key => $value):
                if ($value != ''):
                    $master_password[$key] = do_hash($value);
                endif;
            endforeach;
            if ($this->_update_master_password($master_password)):
                $state['master_password'] = TRUE;
            else:
                $state['master_password'] = FALSE;
            endif;
        endif;

        if ( ! empty($normal_password) && count($normal_password) === 3):
            foreach ($normal_password as $key => $value):
                if ($value != ''):
                    $normal_password[$key] = do_hash($value);
                endif;
            endforeach;
            if ($this->_update_normal_password($normal_password)):
                $state['normal_password'] = TRUE;
            else:
                $state['normal_password'] = FALSE;
            endif;
        endif;

        if ( ! empty($master_email) && count($master_email) === 3):
            if ($this->_update_master_email($master_email)):
                $state['master_email'] = TRUE;
            else:
                $state['master_email'] = FALSE;
            endif;
        endif;

        $this->output->set_content_type('application/json')
            ->set_output(json_encode($state));
    }

    /**
     * Called by `update_settings()`
     *
     * Processes changing of master user pin
     *
     * array keys (current, pin, re_pin)
     *
     * @param array from form
     * @return bool status of operation
     */
    private function _update_master_pin($data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($data)) return FALSE;

        $current= $data['current'];
        $pin = $data['pin'];
        $re_pin = $data['re_pin'];

        $account_type = $this->user->authenticate_user_pin($current);

        if ($account_type != MASTER) return FALSE;

        if (strcmp($pin, $re_pin) == 0):
            return $this->user->update_user_pin(MASTER, $pin);
        else:
            return FALSE;
        endif;
    }

    /**
     * Called by `update_settings()`
     *
     * Processes changing of normal user pin
     *
     * array keys (master_pin, pin, re_pin)
     *
     * @param array from form
     * @return bool status of operation
     */
    private function _update_normal_pin($data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($data)) return FALSE;

        $master_pin = $data['master_pin'];
        $pin = $data['pin'];
        $re_pin = $data['re_pin'];

        $account_type = $this->user->authenticate_user_pin($master_pin);

        if ($account_type != MASTER) return FALSE;

        if (strcmp($pin, $re_pin) == 0):
            return $this->user->update_user_pin(NORMAL, $pin);
        else:
            return FALSE;
        endif;
    }

    /**
     * Called by `update_settings()`
     *
     * Processes changing of master user password
     *
     * Array keys (current, password, re_password)
     *
     * @param array from form
     * @return bool status of operation
     */
    private function _update_master_password($data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($data)) return FALSE;

        $current = $data['current'];
        $password = $data['password'];
        $re_password = $data['re_password'];

        $authorized = $this->user->authenticate_user_password(MASTER, $current);

        if ( ! $authorized) return FALSE;

        if (strcmp($password, $re_password) == 0):
            return $this->user->update_user_password(MASTER, $password);
        else:
            return FALSE;
        endif;
    }

    /**
     * Called by `update_settings()`
     *
     * Processes changing of normal user password
     *
     * Array key (master_password, password, re_password)
     *
     * @param array from form
     * @return bool status of operation
     */
    private function _update_normal_password($data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($data)) return FALSE;

        $master_password = $data['master_password'];
        $password = $data['password'];
        $re_password = $data['re_password'];

        $authorized = $this->user->authenticate_user_password(MASTER, $master_password);

        if ( ! $authorized) return FALSE;

        if (strcmp($password, $re_password) == 0):
            return $this->user->update_user_password(NORMAL, $password);
        else:
            return FALSE;
        endif;
    }

    /**
     * Called by `update_settings()`
     *
     * Processes changing of master user email
     *
     * Array key (master_password, email, re_email)
     *
     * @param array from form
     * @return bool status of operation
     */
    private function _update_master_email($data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($data)) return FALSE;

        $password = do_hash($data['password']);
        $email = $data['email'];
        $re_email = $data['email'];

        $authorized = $this->user->authenticate_user_password(MASTER, $password);

        if ( ! $authorized) return FALSE;

        if (strcmp($email, $re_email) == 0):
            return $this->user->update_user_email(MASTER, $email);
        else:
            return FALSE;
        endif;
    }
}
/* End of file json.php */
/* Location ./app/SmartHome/controllers/json.php */
