<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$lang['error_empty_field']          = "%s must not be empty!";
$lang['error_fail_to_send_email']   = "Fail to send email. Please make sure you're connected to the internet.";
$lang['error_login_password']       = "Authentication Failure! Wrong password!";
$lang['error_login_pin']            = "Authentication Failure! Wrong PIN!";
$lang['error_no_actuators']         = "No Devices Found. Please correct settings!";
$lang['error_no_permission']        = "Permission Denied! Please make sure you have sufficient privilege to do this action!";
$lang['error_no_room_specified']    = "Please choose a room";
$lang['error_no_rooms']             = "No rooms found in the database. Please configure!";
$lang['error_no_auto_state']        = "Automation state not found!";
$lang['error_no_rw_permission']     = "No R/W permission.";
$lang['error_no_user_found']        = "No user found!";
$lang['error_not_allowed']          = "Action not allowed! AJAX request only.";
$lang['error_not_logged_in']        = "Action not allowed! Please log in first.";
$lang['error_pin_length']           = "%s is normally 4 characters long!";
$lang['error_name_exists']          = "%s has already been used! You should use something different.";
$lang['error_socket_create']        = "Failed to create socket.";
$lang['error_socket_connect']       = "Failed to connect to socket.";
$lang['error_uri_request']          = "URI Request Error. Please correct and try again.";
$lang['error_invalid_input']        = "%s is weird.";
$lang['error_no_preset']            = "There isn't any preset available yet! Why not create one?";
$lang['error_wrong_email']          = "Incorrect Email given!";

/* End of file error_lang.php */
/* Location: ./app/SmartHome/language/english/error_lang.php */
