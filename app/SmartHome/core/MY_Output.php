<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Extended Core Input class
 *
 * @package     SmartHome
 * @subpackage  Output
 * @category    Core
 * @author      Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class MY_Output extends CI_Output {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Disable browser caching for certain pages
     */
    public function no_cache()
    {
        $this->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->set_header("Cache-Control: post-check=0, pre-check=0");
        $this->set_header("Pragma: no-cache");
    }
}

/* End of file MY_Output.php */
/* Location: ./app/SmartHome/core/MY_Output.php */
