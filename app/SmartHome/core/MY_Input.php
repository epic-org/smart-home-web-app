<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Extended Core Input class
 *
 * @package     SmartHome
 * @subpackage  Input
 * @category    Core
 * @author      Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class MY_Input extends CI_Input {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * Clean Keys
     *
     * This is a helper function. To prevent malicious users
     * from trying to exploit keys we make sure that keys are
     * only named with alpha-numeric text and a few other items.
     * 
     * Extended to allow: 
     * - '.' (dot), 
     * - '[' (open bracket),
     * - ']' (close bracket)
     *
     * @access  private
     * @param   string
     * @return  string
     */
    function _clean_input_keys($str)
    {
        if (!preg_match("/^[a-z0-9:_\/\.\[\]-]+$/i", $str)):
            /**
             * Check for Development enviroment - Non-descriptive 
             * error so show me the string that caused the problem 
             */
            if (defined('ENVIRONMENT') && ENVIRONMENT == 'development')
                var_dump($str);
            exit('Disallowed Key Characters.');
        endif;

        // Clean UTF-8 if supported
        if (UTF8_ENABLED === TRUE)
            $str = $this->uni->clean_string($str);

        return $str;
    }
}

/* End of file MY_Input.php */
/* Location: ./app/SmartHome/core/MY_Input.php */
