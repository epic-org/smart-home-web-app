<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|--------------------------------------------------------------------------
| Web App Constants
|--------------------------------------------------------------------------
|
| These are used globally in the web app
|
*/

// Web App title
define('TITLE', 'Smart Home');

define('ONE_DAY', 86400);

// Web App's permission level
define('MASTER', 3);
define('NORMAL', 2);
define('GUEST', 1);

// Web App's different types of actuators
define('LIGHT', 1);
define('MOTOR', 2);
define('AC', 3);

// Web App's actuator value
define('ON', 1);
define('OFF', 0);
define('UP', 1);
define('DOWN', 0);
define('STOP', 2);
define('OPEN', 1);
define('CLOSE', 0);
define('POWER', 3);

// Web App View Class Constants
define('MASTER_ONLY', 'master-user');
define('MASTER_HIDE', 'master-user-hide');
define('NORMAL_ONLY', 'normal-user');
define('NORMAL_HIDE', 'normal-user-hide');

define('SOCKET_HOST', 'localhost');
define('SOCKET_PORT', 56765);
/* End of file constants.php */
/* Location: ./app/SmartHome/config/constants.php */
