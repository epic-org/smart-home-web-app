<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Used for enabling and disabling profiler based on environment
$config['profiling'] = FALSE;

/* End of file profiling.php */
/* Location: ./app/SmartHome/config/development/profiling.php */

