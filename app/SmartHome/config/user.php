<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['modules'] = array('user_master', 'user_normal', 'user_guest');

/* End of file user.php */
/* Location: ./app/SmartHome/config/user.php */
