<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Used for comparing users with local LAN
$config['ips'] = array(
    array('start' => ip2long('10.0.0.0'), 'end' => ip2long('10.255.255.255')),
    array('start' => ip2long('172.16.0.0'), 'end' => ip2long('172.31.255.255')),
    array('start' => ip2long('192.168.0.0'), 'end' => ip2long('192.168.255.255')),
    array('start' => ip2long('127.0.0.0'), 'end' => ip2long('127.255.255.255'))
);

/* End of file private_ip.php */
/* Location: ./app/SmartHome/config/private_ip.php */

