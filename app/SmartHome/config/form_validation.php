<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
    '_login_pin' => array(
        array(
            'field' => 'pin',
            'label' => 'PIN',
            'rules' => 'required|exact_length[4]'
        )
    ),

    '_login_password' => array(
        array(
            'field' => 'account_type',
            'label' => 'User Type',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        )
    ),

    'new_preset' => array(
        array(
            'field' => 'preset_name',
            'label' => 'Preset Name',
            'rules' => 'required|is_unique[preset.name]'
        ),
    ),

    'edit_preset' => array(
        array(
            'field' => 'preset_name',
            'label' => 'Preset Name',
            'rules' => 'is_unique[preset.name]'
        ),
    ),

    'new_auto' => array(
        array(
            'field' => 'automation_name',
            'label' => 'Automation Name',
            'rules' => 'required|is_unique[automation.name]'
        ),
        array(
            'field' => 'sensors[sensor_id]',
            'label' => 'Sensor',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'presets[0][preset_id]',
            'label' => 'Preset',
            'rules' => 'required|integer'
        ),
    ),

    'edit_auto' => array(
        array(
            'field' => 'automation_name',
            'label' => 'Automation Name',
            'rules' => 'is_unique[automation.name]'
        ),
        array(
            'field' => 'sensors[sensor_id]',
            'label' => 'Sensor',
            'rules' => 'required|integer'
        ),
        array(
            'field' => 'presets[0][preset_id]',
            'label' => 'Preset',
            'rules' => 'required|integer'
        ),
    ),

    'new_schedule' => array(
        array(
            'field' => 'schedule[name]',
            'label' => 'Schedule Name',
            'rules' => 'required|is_unique[schedule_task.name]'
        ),
        array(
            'field' => 'schedule[time]',
            'label' => 'Time',
            'rules' => 'required'
        ),
        array(
            'field' => 'preset_id',
            'label' => 'Preset',
            'rules' => 'required|integer'
        ),
    ),

    'edit_schedule' => array(
        array(
            'field' => 'schedule[name]',
            'label' => 'Schedule Name',
            'rules' => 'is_unique[schedule_task.name]'
        ),
        array(
            'field' => 'schedule[time]',
            'label' => 'Time',
            'rules' => 'required'
        ),
        array(
            'field' => 'preset_id',
            'label' => 'Preset',
            'rules' => 'required|integer'
        ),
    ),
);

/* End of file form_validation.php */
/* Location: ./app/SmartHome/config/form_validation.php */
