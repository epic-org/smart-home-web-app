<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Cronjob library that formats php dates into cronjob format
 *
 * Crontab format as as follow
 *
 * minute | hour | day-of-month | month | day-of-week | command to execute
 *
 * seperated by space instead of `|`
 *
 * @package     SmartHome
 * @subpackage  Libraries
 * @category    Cronjob
 * @author      Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */

class Cron
{
    // Day value for [Sun, Mon, Tue, Wed, Thu, Fri, Sat]
    public static $day_value = array(64, 32, 16, 8, 4, 2, 1);

    // CI instance
    private $CI;

    // format of the date/time being passed in.
    private $date_format = 'Y-m-d';
    private $time_format = 'H:i:s';
    private $schedule_id;

    private $date;
    private $time;
    private $cron;

    private $index_path;
    private $cache_path;

    /**
     * When intializing this library, user must set the date and time format
     * to convert any time or date strings to date types in any of the functions
     *
     * @param str $params[0] = date_format
     * @param str $params[1] = time_format
     * @param int $params[2] = schedule_id
     */
    public function __construct()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->CI =& get_instance();

        // path to index.php
        $this->index_path = FCPATH.SELF;

        // path to cache folder
        $this->cache_path = ($this->CI->config->item('cache_path') != '') ? $this->CI->config->item('cache_path') : APPPATH.'cache/';

        $this->cron = ltrim(shell_exec('crontab -l'), "\n");
    }

    /**
     * Build the cron string for schedule
     *
     * @param array schedule
     * @param int preset id
     * @return str cron string
     */
    public function build_cron_str($schedule = array(), $preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->time = DateTime::createFromFormat($this->time_format, $schedule['time']);

        $cron = '';

        // time cannot be parsed. Return fail!
        if ($this->time == FALSE) return FALSE;

        // at specific minute and hour
        $cron .= $this->time->format('i G ');

        if ( ! isset($schedule['date'])):
            // any day-of-month and any month
            $cron .= '* * ';

        else:
            $this->date = DateTime::createFromFormat($this->date_format, $schedule['date']);

            // date cannot be parsed. Return fail!
            if ($this->date == FALSE) return FALSE;

            $cron .= $this->date->format('j n ');

        endif;

        $cron .= $this->get_days($schedule['day']);

        $cron .= $this->build_cli($preset_id);

        return $cron;
    }

    /**
     * Get schedule data and converts adds the schedule to cron
     *
     * @param array schedule
     * @param int preset id to execute
     * @param int schedule id
     * @param str date format in array
     * @param str time format in array
     * @return bool status of operation
     */
    public function edit_schedule($schedule = array(), $preset_id = FALSE, $schedule_id = FALSE, $date_format = FALSE, $time_format = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if ($preset_id === FALSE) return FALSE;
        if ($schedule_id === FALSE) return FALSE;
        if ($date_format != FALSE) $this->date_format = $date_format;
        if ($time_format != FALSE) $this->time_format = $time_format;

        $this->schedule_id = $schedule_id;

        $cron = $this->build_cron_str($schedule, $preset_id);

        return $this->edit_cron($cron, $schedule_id);
    }

    /**
     * Get schedule data and converts adds the schedule to cron
     *
     * @param array schedule
     * @param int preset id to execute
     */
    public function add_schedule($schedule = array(), $preset_id = FALSE, $schedule_id = FALSE, $date_format = FALSE, $time_format = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if ($preset_id === FALSE) return FALSE;
        if ($schedule_id === FALSE) return FALSE;
        if ($date_format != FALSE) $this->date_format = $date_format;
        if ($time_format != FALSE) $this->time_format = $time_format;

        $this->schedule_id = $schedule_id;

        $cron = $this->build_cron_str($schedule, $preset_id);

        return $this->append_cron($cron);
    }

    /**
     * Append to cron
     *
     * @param str
     * @return bool status of operation
     */
    public function append_cron($str = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if ($str === FALSE) return FALSE;

        $this->cron .= $str;

        return $this->set_cron();
    }

    /**
     * Edit cron line with schedule id
     *
     * @param str replacement line
     * @param int schedule id
     * @return bool status of operation
     */
    public function edit_cron($str = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if ($str === FALSE) return FALSE;

        $schedule = '#'.$this->schedule_id.'_';
        $schedule_pos = strpos($this->cron, $schedule);

        // this schedule doesn't exist
        if ($schedule_pos === FALSE) return FALSE;

        // find the previous line's new line character
        $start = strrpos($this->cron, "\n", $schedule_pos - strlen($this->cron));

        // if can't find newline means it's the first line!
        if ($start === FALSE) $start = -1;

        // replace the line with our str
        $new = substr($this->cron, 0, $start + 1)
            . $str
            . substr($this->cron, $schedule_pos + strlen($schedule));

        $this->cron = rtrim($new, "\n");
        return $this->set_cron();
    }

    /**
     * Set the cronjob
     *
     * @param str
     * @return bool status of operation
     */
    public function set_cron()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $output = array();

        exec("echo \"{$this->cron}\" | crontab", $output, $ret_value);

        if ($ret_value === 0) return TRUE;
        else return FALSE;
    }

    /**
     * Deleting cronjob
     *
     * cronjob schedule id are identified by "#id_#
     *
     * @param int schedule id
     * @return bool status of operation
     */
    public function delete_schedule($schedule_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $schedule = '#'.$schedule_id.'_';
        $schedule_pos = strpos($this->cron, $schedule);

        // this schedule doesn't exist
        if ($schedule_pos === FALSE) return FALSE;

        // find the previous line's new line character
        $start = strrpos($this->cron, "\n", $schedule_pos - strlen($this->cron));

        // if can't find newline means it's the first line!
        if ($start === FALSE) $start = -1;

        // copy substr and leaving the entire line out + the newline too
        $new = substr($this->cron, 0, $start + 1) . substr($this->cron, $schedule_pos + strlen($schedule) + 1);

        $this->cron = rtrim($new, "\n");
        return $this->set_cron();
    }

    /**
     * Toggling cronjob enabled status
     *
     * cronjob schedule are identified by "#id_"
     * cronjob schedule commented out are identified by "#id#"
     *
     * @param int schedule id
     * @param bool enabled
     * @return bool status of operation
     */
    public function toggle_schedule($schedule_id = FALSE, $enabled = TRUE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        // find where the line with #schedule_id is
        $schedule = '#'.$schedule_id.'_';
        $schedule_pos = strpos($this->cron, $schedule);

        $comment = '#'.$schedule_id.'#';
        $comment_pos = strpos($this->cron, $comment);

        if ($enabled == TRUE):
            // Check if comment exist, if it's not, return FALSE
            if ($comment_pos === FALSE) return FALSE;

            // take the substr without the comment
            $new = substr($this->cron, 0, $comment_pos) . substr($this->cron, $comment_pos + strlen($comment));

        else:

            // this schedule doesn't exist!
            if ($schedule_pos === FALSE) return FALSE;

            // then find the position of the newline of that schedule
            $start = strrpos($this->cron, "\n", $schedule_pos - strlen($this->cron));

            // check if position exist. if not exist, it's the first line!
            if ($start === FALSE) $start = -1;

            // add a `#` to comment out that line
            $new = substr($this->cron, 0, $start + 1) .'#'.$schedule_id.'#'. substr($this->cron, $start + 1);

        endif;

        $this->cron = rtrim($new, "\n"); //removes extra newline
        return $this->set_cron();
    }

    /**
     * Builds the cli string for cronjob
     *
     * @param int preset id to execute
     */
    public function build_cli($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $str = "php {$this->index_path} cli run_preset {$preset_id} #{$this->schedule_id}_";

        return $str;
    }

    /**
     * Return string of days for cronjob
     *
     * @param int  7-bit binary converted to int
     * @return str
     */
    private function get_days($days = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $str_arr = array();

        $tmp = $days;
        $i = 0;

        if (empty($days)) return "* ";

        // loops by subtracting days with day_value array
        // Then build string with $i because 0 is Sun and 6 is Sat
        while($days > 0 && $i < 7):
            $day = self::$day_value[$i];

            $tmp -= $day;
            if ($tmp >= 0):
                $str_arr[] = $i;
                $days = $tmp;
            else:
                $tmp = $days;
            endif;

            $i++;
        endwhile;

        // return string of days sperated by 'comma' plus a space
        return implode(',', $str_arr) . ' ';
    }
}

/* End of file Cron.php */
/* Location: app/SmartHome/libraries/Cron.php */

