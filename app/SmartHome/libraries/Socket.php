<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Socket library that communicates with the Python Socket
 * to command the microcontroller.
 *
 * @package     SmartHome
 * @subpackage  Libraries
 * @category    Socket
 * @author      Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */

class Socket
{
    // The controller instance
    private $CI;

    private $host = 'localhost';
    private $port = '54579';

    // Final output to socket
    private $command_out = '';
    // Stores return value (if any)
    private $value = '';
    // Stores socket instance
    private $socket = NULL;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    /**
     * This function creates a socket and sends the command to the microcontroller.
     *
     * The Command Header format for actuators should be:-
     *  [Request Type] + [ID] + [Value]
     *  e.g. "A 3 1" to send command 1 to actuator id 3.
     *
     * The Command Header format for sensors should be:-
     *  [Request Type] + [ID]
     *  e.g. "S 1" to get sensor reading for id 1.
     *
     *  Socket returns 'DONE' if actuator command is sent.
     *  Socket returns a value if sensor reading is requested.
     *  Socket returns 'FAIL' if command failed to execute.
     *
     * @param char request type (A for actuator, S for sensor)
     * @param int actuator/sensor id
     * @param int value to send
     * @param str host [Optional]
     * @param str post [Optional]
     * @return int value of sensor reading (if any)
     */
    public function send($request_type = FALSE, $id = FALSE, $value='0', $compare=NULL)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if ($request_type === FALSE OR $id === FALSE) return FALSE;

        if (defined('SOCKET_HOST')) $this->host = SOCKET_HOST;
        if (defined('SOCKET_PORT')) $this->port = SOCKET_PORT;

        if ($request_type{0} === 'S'):
            $this->command_out = "S {$id}";

        elseif ($request_type{0} === 'A'):
            $this->command_out = "A {$id} {$value}";

        elseif ($request_type{0} === 'T'):
            $this->command_out = "T {$id} {$value} {$compare}";

        else:
            return FALSE;

        endif;

        $this->socket = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

        if($this->socket === FALSE):
            log_message('error', $this->CI->lang->line('error_socket_create') . ' Reason : ' . socket_strerror(socket_last_error()));
            return FALSE;

        else:
            log_message('info', 'Socket created!');
            log_message('info', "Attempting to connect to {$this->host} on port {$this->port}");
            $result = @socket_connect($this->socket, $this->host, $this->port);

            if ($result === false):
                log_message('error', $this->CI->lang->line('error_socket_connect')
                    . " Reason : " . socket_strerror(socket_last_error($this->socket)));
                return FALSE;

            else:
                log_message('info', 'Connection established');
                log_message('info', "Sending command: {$this->command_out}");

                socket_write($this->socket, $this->command_out, strlen($this->command_out));
                log_message('info', "Command sent");
                $this->value = socket_read($this->socket, 1024);
                socket_close($this->socket);

                log_message('info', "Received: {$this->value}");
                return $this->value;

            endif; //result
        endif; // socket

    } // End function socket_s
}

/* End of file Socket.php */
/* Location: app/SmartHome/libraries/Socket.php */

