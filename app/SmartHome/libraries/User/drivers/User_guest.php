<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Guest User Class
 *
 * This class is the sub-class of User class.
 * This class allows control on the Guest User level access.
 *
 * @package		SmartHome
 * @subpackage	Drivers
 * @category	Libraries
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class User_guest extends CI_Driver {

    /**
     * Get a room
     *
     * @param int room id
     * @return room object
     */
    public function get_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(GUEST);

        $this->CI->load->model('room');

        return $this->CI->room->get($room_id);
    }

    /**
     * Get list of rooms to display
     *
     * @return array of rooms object
     */
    public function get_all_rooms()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(GUEST);

        $this->CI->load->model('room');

        return $this->CI->room->get_all();
    }

    /**
     * Get all actuators by room id group
     * them by actuator type
     *
     * @param int room_id
     * @return array of actuator by type
     */
    public function get_actuators_by_type($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(GUEST);

        $this->CI->load->model('actuator');

        $actuators[LIGHT] = $this->CI->actuator->get_by_room($room_id, LIGHT);
        $actuators[MOTOR] = $this->CI->actuator->get_by_room($room_id, MOTOR);
        $actuators[AC] = $this->CI->actuator->get_by_room($room_id, AC);

        return $actuators;
    }

    /**
     * Get all actuators by room id
     *
     * @param int room_id
     * @return array of actuator
     */
    public function get_actuators_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(GUEST);

        $this->CI->load->model('actuator');

        $actuators = $this->CI->actuator->get_by_room($room_id);

        return $actuators;
    }

    /**
     * Send command to actuators
     *
     * @param char request type
     * @param int actuator id
     * @param int actuator command value
     * @return bool status of operation
     */
    public function send_command($request_type = FALSE, $actuator_id = FALSE, $value = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(GUEST);

        if (empty($request_type) OR empty($actuator_id) OR $value === FALSE)
            return FALSE;

        return $this->CI->socket->send($request_type, $actuator_id, $value);
    }

    /**
     * Get room name by id
     *
     * @param int room id
     * @return associative array
     */
    public function get_room_name($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(GUEST);

        $this->CI->load->model('room');       
        $room_name = $this->CI->room->get_name($room_id);

        return $room_name;
    }
}

/* End of file User_guest.php */
/* Location: ./app/SmartHome/libraries/User/drivers/User_guest.php */

