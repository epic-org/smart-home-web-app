<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Master User Class
 *
 * This class is the sub-class of User class.
 * This class allows control on the Master User level access.
 *
 * @package		SmartHome
 * @subpackage	Drivers
 * @category	Libraries
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class User_master extends CI_Driver {

    /**
     * Update user's pin by account type
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str pin SHA1 Hashed
     * @return bool status of operation
     */
    public function update_user_pin($account_type = FALSE, $pin = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('user_account');

        return $this->CI->user_account->update_pin($account_type, $pin);
    }

    /**
     * Update user's password by account type
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str pin SHA1 Hashed
     * @return bool status of operation
     */
    public function update_user_password($account_type = FALSE, $password = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('user_account');

        return $this->CI->user_account->update_password($account_type, $password);
    }

    /**
     * Update user's email by account type
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str email
     * @return bool status of operation
     */
    public function update_user_email($account_type = FALSE, $email = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('user_account');

        return $this->CI->user_account->update_email($account_type, $email);
    }

    /**
     * Rename room by id
     *
     * @param int room id
     * @param str new room name
     * @return TRUE if successful, FALSe otherwise
     */
    public function rename_room($room_id = FALSE, $name = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('room');

        return $this->CI->room->edit($room_id, $name);
    }

    /**
     * Rename actuator by id
     *
     * @param int actuator id
     * @param str actuator name
     * @return success or fail depending on operation
     */
    public function rename_actuator($actuator_id = FALSE, $name = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('actuator');

        return $this->CI->actuator->rename($actuator_id, $name);
    }

    /**
     * Adds new preset to database
     *
     * @param str name of preset
     * @param int room id
     * @param array of associative array, values of actuators
     */
    public function add_preset($name = '', $room_id = FALSE, $preset_data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('preset');

        return $this->CI->preset->add($name, $room_id, $preset_data);
    }

    /**
     * Edit preset by id
     *
     * @param int preset id
     * @param str name of preset
     * @param array of associate array of actuator values
     * @return TRUE if successful, FALSE otherwise
     */
    public function edit_preset($preset_id = FALSE, $name = '', $data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('preset');

        return $this->CI->preset->edit($preset_id, $name, $data);
    }

    /**
     * Delete existing preset from database by id
     *
     * @param int preset id
     * @return bool status of deletion
     */
    public function delete_preset($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('preset');

        return $this->CI->preset->delete($preset_id);
    }

    /**
     * Add new automation to database
     *
     * @param str name
     * @param int room id
     * @param array of preset values
     * @param array of sensor values
     * @return bool operation status
     */
    public function add_automation($name = '', $room_id = FALSE, $presets = array(), $sensors = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('automation');

        return $this->CI->automation->add($name, $room_id, $presets, $sensors);
    }

    /**
     * Delete automation from database by id
     *
     * @param int automation id
     * @return bool status of deletion
     */
    public function delete_automation($automation_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('automation');

        return $this->CI->automation->delete($automation_id);
    }

    /**
     * Edit automation by id
     *
     * @param int automation id
     * @param str new name
     * @param array of preset values
     * @param array of sensor values
     * @return bool operation status
     */
    public function edit_automation($automation_id = FALSE, $name = '', $presets = array(), $sensors = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('automation');

        return $this->CI->automation->edit($automation_id, $name, $presets, $sensors);
    }

    /**
     * Add new schedule task
     *
     * @param array of schedule data [name] [day] [time] [date]
     * @param int preset id to trigger
     * @param int room id
     * @return int new schedule id
     */
    public function add_schedule($schedule = array(), $preset_id = FALSE, $room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('schedule_task');

        return $this->CI->schedule_task->add($schedule, $preset_id, $room_id);
    }

    /**
     * Edit schedule by id
     *
     * @param int schedule id
     * @param array of schedule data
     * @param int preset id
     * @return associative array of schedule data
     */
    public function edit_schedule($schedule_id = FALSE, $schedule = array(), $preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('schedule_task');

        return $this->CI->schedule_task->edit($schedule_id, $schedule, $preset_id);
    }

    /**
     * Delete existing schedule from database by id
     *
     * @param int schedule id
     * @return bool status of deletion
     */
    public function delete_schedule($schedule_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('schedule_task');

        return $this->CI->schedule_task->delete($schedule_id);
    }
}

/* End of file User_master.php */
/* Location: ./app/SmartHome/libraries/User/drivers/User_master.php */

