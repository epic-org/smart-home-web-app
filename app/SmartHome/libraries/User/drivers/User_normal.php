<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * SmartHome Normal User Class
 *
 * This class is the sub-class of User class.
 * This class allows control on the Normal User level access.
 *
 * @package		SmartHome
 * @subpackage	Drivers
 * @category	Libraries
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class User_normal extends CI_Driver {

    /**
     * Get a preset
     *
     * @param int preset id
     * @return preset object
     */
    public function get_preset($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);

        $this->CI->load->model('preset');

        return $this->CI->preset->get($preset_id);
    }

    /**
     * Get state of actuator in preset
     *
     * @param int preset id
     * @return associative array with actuator id and value
     */
    public function get_preset_state($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(MASTER);
        $this->CI->load->model('preset');

        return $this->CI->preset->get_state($preset_id);
    }

    /**
     * Get presets by room id
     *
     * @param int room id
     * @return array of preset objects
     */
    public function get_presets_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);
        $this->CI->load->model('preset');

        return $this->CI->preset->get_by_room($room_id);
    }

    /**
     * Get automations by room id
     *
     * @param int room id
     * @return array of automation objects
     */
    public function get_automations_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);
        $this->CI->load->model('automation');

        return $this->CI->automation->get_by_room($room_id);
    }

    /**
     * Get sensors by room id
     *
     * @param int room id
     * @return array of sensor objects
     */
    public function get_sensors_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);
        $this->CI->load->model('sensor');

        return $this->CI->sensor->get_by_room($room_id);
    }

    /**
     * Get automation state by id
     *
     * @param int automation id
     * @return associative array of automation data
     */
    public function get_automation_state($automation_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);
        $this->CI->load->model('automation');

        return $this->CI->automation->get_state($automation_id);
    }

    /**
     * This function gets sensor reading and returns the value
     *
     * @param char request type
     * @param int sensor id
     * @return double sensor reading
     */
    public function get_sensor_reading($request_type = FALSE, $sensor_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);

        if ($request_type === FALSE OR $sensor_id === FALSE)
            return FALSE;

        return $this->CI->socket->send($request_type, $sensor_id);
    }

    /**
     * Get schedules by room id
     *
     * @param int room id
     * @return array of schedules objects
     */
    public function get_schedules_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);
        $this->CI->load->model('schedule_task');

        return $this->CI->schedule_task->get_by_room($room_id);
    }

    /**
     * Get schedule state by id
     *
     * @param int schedule id
     * @return associative array of schedule data
     */
    public function get_schedule_state($schedule_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);
        $this->CI->load->model('schedule_task');

        return $this->CI->schedule_task->get_state($schedule_id);
    }

    /**
     * Updates the status of automation by id
     *
     * @param int automation id
     * @param bool enabled
     * @return bool status of operation
     */
    public function update_automation_status($automation_id = FALSE, $enabled = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);
        $this->CI->load->model('automation');

        return $this->CI->automation->update_status($automation_id, $enabled);
    }

    /**
     * Updates the status of schedule task by id
     *
     * @param int schedule task id
     * @param bool enabled
     * @return bool status of operation
     */
    public function update_schedule_status($schedule_task_id = FALSE, $enabled = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->check_authority(NORMAL);
        $this->CI->load->model('schedule_task');

        return $this->CI->schedule_task->update_status($schedule_task_id, $enabled);
    }
}

/* End of file User_normal.php */
/* Location: ./app/SmartHome/libraries/User/drivers/User_normal.php */

