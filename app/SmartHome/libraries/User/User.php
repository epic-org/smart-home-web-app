<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * SmartHome User Class
 *
 * This class is the parent class for managing user access.
 *
 * @package		SmartHome
 * @subpackage	Drivers
 * @category	Libraries
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */

class User extends CI_Driver_Library {
    public $valid_drivers;
    public $CI;

    public function __construct()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->CI =& get_instance();
        $this->CI->config->load('user', TRUE);
        $this->valid_drivers = $this->CI->config->item('modules', 'user');
    }

    /**
     * Verify master user's email
     *
     * @permission all
     *
     * @param str master user email
     * @return str master user email
     */
    public function get_master_email($email = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->CI->load->model('user_account');

        return $this->CI->user_account->get_master_email($email);
    }

    /**
     * Authenticate user with pin code.
     *
     * @permission all
     *
     * @param str pin SHA1 Hashed
     * @return int account_type , FALSE if fail.
     */
    public function authenticate_user_pin($pin = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->CI->load->model('user_account');

        return $this->CI->user_account->get_user_by_pin($pin);
    }

    /**
     * Authenticate user with password.
     *
     * @permission all
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str password SHA1 Hashed
     * @return bool TRUE if successful
     */
    public function authenticate_user_password($account_type = FALSE, $password = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->CI->load->model('user_account');

        return $this->CI->user_account->get_user_by_password($account_type, $password);
    }

    /**
     * Update the reset pin code
     *
     * @permission all
     *
     * @param str md5
     * @return bool status of operation
     */
    public function update_master_pin($rand = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->CI->load->model('user_account');

        return $this->CI->user_account->update_pin(MASTER, $rand);
    }

    /**
     * Update the reset password code
     *
     * @permission all
     *
     * @param str md5
     * @return bool status of operation
     */
    public function update_master_password($rand = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->CI->load->model('user_account');

        return $this->CI->user_account->update_password(MASTER, $rand);
    }

    /**
     * Update user's pin by account type
     *
     * @permission master
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str pin SHA1 Hashed
     * @return bool status of operation
     */
    public function update_user_pin($account_type = FALSE, $pin = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->update_user_pin($account_type, $pin);
    }

    /**
     * Update user's password by account type
     *
     * @permission master
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str pin SHA1 Hashed
     * @return bool status of operation
     */
    public function update_user_password($account_type = FALSE, $password = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->update_user_password($account_type, $password);
    }

    /**
     * Update user's email by account type
     *
     * @permission master
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str email
     * @return bool status of operation
     */
    public function update_user_email($account_type = FALSE, $email = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->update_user_email($account_type, $email);
    }

    /**
     * Get a room
     *
     * @permission guest
     *
     * @param int room id
     * @return room object
     */
    public function get_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->guest->get_room($room_id);
    }

    /**
     * Rename room by id
     *
     * @permission master
     *
     * @param int room id
     * @return TRUE upon succes, FALSE othrewise
     */
    public function rename_room($room_id = FALSE, $name = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->rename_room($room_id, $name);
    }

    /**
     * Get list of rooms
     *
     * @permission guest
     */
    public function get_all_rooms()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->guest->get_all_rooms();
    }

    /**
     * Rename actuator by id
     *
     * @permission master
     *
     * @param int actuator id
     * @param str new name
     * @return bool status of operation
     */
    public function rename_actuator($actuator_id = FALSE, $name = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->rename_actuator($actuator_id, $name);
    }

    /**
     * Get all actuators by room id grouping
     * them by type of actuators
     *
     * @permission guest
     *
     * @param int room_id
     * @return array of actuator objects by type
     */
    public function get_actuators_by_type($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->guest->get_actuators_by_type($room_id);
    }

    /**
     * Get all actuators by room id
     *
     * @permission guest
     *
     * @param int room_id
     * @return array of actuator objects
     */
    public function get_actuators_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->guest->get_actuators_by_room($room_id);
    }

    /**
     * Get preset by id
     *
     * @permission normal
     *
     * @param int preset id
     * @return preset object
     */
    public function get_preset($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->get_preset($preset_id);
    }

    /**
     * Get all presets in room
     *
     * @permission normal
     *
     * @param int room id
     * @return array of presets object
     */
    public function get_presets_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->get_presets_by_room($room_id);
    }

    /**
     * Renames actuator by id
     *
     * @permission master
     *
     * @param int actuator id
     * @return bool operation status
     */
    public function rename_preset($actuator_id = FALSE, $name = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->rename_preset($actuator_id, $name);
    }

    /**
     * Adds new preset to database.
     *
     * @permission master
     *
     * @param str name of preset
     * @param int room id
     * @param array of associate array of actuator values
     * @return bool operation status
     */
    public function add_preset($name = '', $room_id = FALSE, $preset_data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->add_preset($name, $room_id, $preset_data);
    }

    /**
     * Edit preset by id
     *
     * @permission master
     *
     * @param int preset id
     * @param str name of new preset
     * @param array of associate array of actuator values
     * @return bool operation status
     */
    public function edit_preset($preset_id = FALSE, $name = '', $data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->edit_preset($preset_id, $name, $data);
    }

    /**
     * Get actuator's state in preset
     *
     * @permission normal
     *
     * @param int preset id
     * @return array of object with actuator id and value
     */
    public function get_preset_state($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->get_preset_state($preset_id);
    }

    /**
     * Delete preset from database by id
     *
     * @permission master
     *
     * @param int preset id
     * @return bool operation status
     */
    public function delete_preset($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->delete_preset($preset_id);
    }

    /**
     * Get all automation in room
     *
     * @permission normal
     *
     * @param int room id
     * @return array of automation objects
     */
    public function get_automations_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->get_automations_by_room($room_id);
    }

    /**
     * Get sensors by room for automation
     *
     * @permission normal
     *
     * @param int room id
     * @return array of sensor object in room
     */
    public function get_sensors_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->get_sensors_by_room($room_id);
    }

    /**
     * Add a new automation to database
     *
     * @permission master
     *
     * @param str automation name
     * @param int room id
     * @param array of preset values
     * @param array of sensor values
     * @return bool operation status
     */
    public function add_automation($name = '', $room_id = FALSE, $presets = array(), $sensors = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->add_automation($name, $room_id, $presets, $sensors);
    }

    /**
     * Delete automation from database by id
     *
     * @permission master
     *
     * @param int automation id
     * @return bool operation status
     */
    public function delete_automation($automation_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->delete_automation($automation_id);
    }

    /**
     * Get the state of automation
     *
     * @permission normal
     *
     * @param int automation_id
     * @return associative array of automation data
     */
    public function get_automation_state($automation_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->get_automation_state($automation_id);
    }

    /**
     * Edit current automation by id
     *
     * @permission master
     *
     * @param int automation id
     * @param str new name
     * @param array of preset values
     * @param array of sensor values
     * @return bool operation status
     */
    public function edit_automation($automation_id = FALSE, $name = '', $presets = array(), $sensors = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->edit_automation($automation_id, $name, $presets, $sensors);
    }

    /**
     * This function  sends the command to actuator id
     *
     * @permission guest
     *
     * @param char request type
     * @param int actuator id
     * @param int command value
     * @return bool status of operation
     */
    public function send_command($request_type = FALSE, $actuator_id = FALSE, $value = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->guest->send_command($request_type, $actuator_id, $value);
    }

    /**
     * This function gets sensor reading and returns the value
     *
     * @permission normal
     *
     * @param char request type
     * @param int sensor id
     * @return double sensor reading
     */
    public function get_sensor_reading($request_type = FALSE, $sensor_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->get_sensor_reading($request_type, $sensor_id);
    }

    /**
     * Get schedules by room
     *
     * @permission normal
     *
     * @param int room id
     * @return array of schedule objects
     */
    public function get_schedules_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->get_schedules_by_room($room_id);
    }

    /**
     * Get schedule's state
     *
     * @permission normal
     *
     * @param int schedule id
     * @return associative array of schedule data
     */
    public function get_schedule_state($schedule_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->get_schedule_state($schedule_id);
    }

    /**
     * Edit schedule by id
     *
     * @permission master
     *
     * @param int schedule id
     * @param array of schedule data
     * @param int preset id
     * @return bool status of operation
     */
    public function edit_schedule($schedule_id = FALSE, $schedule = array(), $preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->edit_schedule($schedule_id, $schedule, $preset_id);
    }

    /**
     * Add new schedule to database
     *
     * @permission master
     *
     * @param array of schedule data [name] [day] [time] [date]
     * @param int preset id to trigger
     * @param int room id
     * @return int new schedule id
     */
    public function add_schedule($schedule = array(), $preset_id = FALSE, $room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->add_schedule($schedule, $preset_id, $room_id);
    }

    /**
     * Delete schedule from database by id
     *
     * @permission master
     *
     * @param int schedule id
     * @return bool operation status
     */
    public function delete_schedule($schedule_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->master->delete_schedule($schedule_id);
    }

    /**
     * Gets the room name by id
     *
     * @permission guest
     *
     * @param int room id
     * @return associative array
     */
    public function get_room_name($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->guest->get_room_name($room_id);
    }

    /**
     * Updates the status of automation by id
     *
     * @permission normal
     *
     * @param int automation id
     * @param bool enabled
     * @return bool status of operation
     */
    public function update_automation_status($automation_id = FALSE, $enabled = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->update_automation_status($automation_id, $enabled);
    }

    /**
     * Updates the status of schedule task by id
     *
     * @permission normal
     *
     * @param int schedule task id
     * @param bool enabled
     * @return bool status of operation
     */
    public function update_schedule_status($schedule_task_id = FALSE, $enabled = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        return $this->normal->update_schedule_status($schedule_task_id, $enabled);
    }

    /**
     * Checks user permission.
     * Redirects to room list and display error if authorization failure
     *
     * This function acts like a roadblock on functions that requires higher permission.
     * If account_type is >= $permission_level , allow.
     *
     * @param int permission_level
     */
    public function check_authority($permission_level = 0)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if ($this->CI->session->userdata('account_type') < $permission_level)
            show_error($this->CI->lang->line('error_no_permission'), 403);
    }
}

/* End of file User.php */
/* Location: ./app/SmartHome/libraries/User/User.php */

