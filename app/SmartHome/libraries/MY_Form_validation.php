<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Form Validation library extension
 *
 * This class enables form validation to also perform
 * query to check for duplicate entry in the database.
 *
 * @package		SmartHome
 * @subpackage	Form Validation
 * @category	Libraries
 * @author		Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class MY_Form_validation extends CI_Form_validation {

    function unique($value, $params) {

        $CI =& get_instance();
        $CI->load->database();

        $CI->form_validation->set_message('unique',
            'The %s is already being used.');

        list($table, $field) = explode(".", $params, 2);

        $query = $CI->db->select($field)->from($table)
            ->where($field, $value)->limit(1)->get();

        if ($query->row())
            return false;

        else
            return true;
    }
}

/* End of file MY_Form_validation.php */
/* Location: ./app/SmartHome/libraries/MY_Form_validation.php */
