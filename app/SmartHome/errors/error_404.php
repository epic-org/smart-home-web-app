<!DOCTYPE html>
<html lang="en">
<head>
	<title>Smart Home!</title>
	<link rel="stylesheet" href="<?=base_url('public/assets/bootstrap/css/bootstrap.min.css')?>" />
	<link rel="stylesheet" href="<?=base_url('public/css/custom.css')?>" />
	<style type="text/css">
		.door {
			background-image: url(<?=base_url('public/img/open_reddoor.png')?>);
			background-repeat: no-repeat;
			background-position: bottom right;
			background-size: 25%;
		}

		.jumbotron {
			color: #ffffff;
			background-color: rgba(0, 0, 0, 0.5);
		}

		.list-group {
			color: #000000;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="door">
			<div class="jumbotron">
				<h1>Oops!</h1> 
				<h2>Do you have the wrong room number?</h2>
				<p>We can't seem to find that room! (Error <?=$heading?>)</p>
				<hr />
				<h2>Would you like us to help you?</h2>
				<ul class="list-group">
					<a href="<?=site_url()?>"><li class="list-group-item">Go to List of Rooms</li></a>
					<a href="<?=site_url('logout')?>"><li class="list-group-item">Go to Login Page</li></a>
	<!-- 				<li class="list-group-item"></li>
					<li class="list-group-item"></li>
					<li class="list-group-item"></li> -->
				</ul>
			</div>
		</div>
	</div>
</body>
</html>