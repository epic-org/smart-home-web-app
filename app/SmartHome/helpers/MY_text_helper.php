<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('format_error'))
{
    /**
     * This function formats the error that's normally supplied by validation_errors()
     * to use bootstrap error format.
     *
     * @param str error
     * @return str new date
     */
    function format_error($error = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($error)) return FALSE;

        $wrap = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>';

        return $wrap . $error . '</div>';
    }
}

/* End of file MY_text_helper.php */
/* Location: ./app/SmartHome/helpers/MY_text_helper.php */

