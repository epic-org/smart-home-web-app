<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_active_days'))
{
    /**
     * This function get the day active stored in decimal
     * and converts it to binary. Then loops through it
     * to display appropriately.
     *
     * @param int days active
     */
    function get_active_days($day)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $_day = array('S', 'M', 'T', 'W', 'T', 'F', 'S');
        $str = '';

        // decimal to binary
        $bin = sprintf( "%07d", decbin($day));

        for ($i = 0; $i < strlen($bin); $i++)
        {
            $str .= '<span class=';

            if ($bin{$i} === '0')
            {
                $str .= '"repeat-inactive">';
            }
            elseif ($bin{$i} === '1')
            {
                $str .= '"repeat-active">';
            }
            else
            {
                $str .= '"error-danger">';
            }

            $str .= " " . $_day[$i] . " ";

            $str .= "</span>";
        }

        return $str;
    }
}

if ( ! function_exists('format_from'))
{
    /**
     * This function allows flexibility to change from a type of date format
     * to another.
     *
     * @param str date
     * @param str original date format
     * @param str format to change to
     * @return str new date
     */
    function format_from($date = FALSE, $from = 'Y-m-d', $to = 'd/m/Y')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $newDate = DateTime::createFromFormat($from, $date);

        if ($newDate === FALSE) return 'Parse Error!';

        return $newDate->format($to);
    }
}

/* End of file MY_date_helper.php */
/* Location: ./app/SmartHome/helpers/MY_date_helper.php */
