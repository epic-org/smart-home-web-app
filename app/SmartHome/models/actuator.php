<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Actuator Model
 *
 * Actuator Model handles every transaction
 * between the database and the controller.
 *
 * @package     SmartHome
 * @category    Model
 * @author      Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Actuator extends CI_Model {

    public $id = '';
    public $name = '';
    public $type = '';
    public $room_id = '';

    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());
    }

    /**
     * Get all actuators with the room_id
     *
     * The $type parameter is optional.
     * If not specified, returns all actuators regardless of type.
     *
     * @param int room_id
     * @param int actuator type
     * @return array of actuator objects
     */
    public function get_by_room($room_id = FALSE, $type = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($room_id)) return array();

        $this->db->from('actuator')->where('room_id', $room_id);

        if ($type !== FALSE)
            $this->db->where('type', $type);

        $query = $this->db->get();

        return $query->result();
    }

    /**
     * Add new actuator to table `actuator`
     *
     * @param int actuator id
     * @param str actuator name
     * @param int room id
     * @return bool FALSE if fail, TRUE if successful
     */
    public function add($id = FALSE, $name = '', $type = FALSE, $room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id) OR empty($name) OR empty($type) OR empty($room_id))
            return FALSE;

        $this->id = $id;
        $this->name = $name;
        $this->room_id = $room_id;

        return $this->db->insert('actuator', $this);
    }

    /**
     * Rename actuator by id
     *
     * @param int actuator id
     * @param str new actuator name
     * @return bool TRUE if successful, FALSE if fail
     */
    public function rename($id = FALSE, $name = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());
        
        if (empty($id) OR empty($name))
            return FALSE;

        $this->db->where('id', $id);
        return $this->db->update('actuator', array('name' => $name));
    }
}

/* End of file actuator.php */
/* Location: ./app/SmartHome/models/actuator.php */
