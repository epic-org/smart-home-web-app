<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Automation extends CI_Model {

    public $id = '';
    public $name = '';
    public $enabled = FALSE;
    public $room_id = '';

    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());
    }

    /**
     * Add a new automation
     *
     * @param str name
     * @param int room id
     * @param array of preset values
     * @param array of sensor values
     * @return bool query result
     */
    public function add($name = '', $room_id = FALSE, $presets = array(), $sensors = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($name) OR empty($room_id))
            return FALSE;

        $this->name = $name;
        $this->room_id = $room_id;
        $this->enabled = TRUE;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();
        $this->db->insert('automation', $this);

        // Get id of last entry
        $this->id = $this->db->insert_id();

        // Loops through the data to add the automation id
        foreach($sensors as $key => $value)
        {
            $sensors[$key]['automation_id'] = $this->id;
        }

        foreach($presets as $key => $value)
        {
            $presets[$key]['automation_id'] = $this->id;
        }

        $this->db->insert_batch('automation_sensor', $sensors);
        $this->db->insert_batch('automation_preset', $presets);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Delete automation from database
     *
     * @param int automation id
     * @return bool status of operation
     */
    public function delete($automation_id = FALSE)
    {
        if (empty($automation_id)) return FALSE;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();

        $this->db->where('automation_id', $automation_id);
        $this->db->delete('automation_preset');

        $this->db->where('automation_id', $automation_id);
        $this->db->delete('automation_sensor');

        $this->db->where('id', $automation_id);
        $this->db->delete('automation');

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Get all automations by room id
     *
     * @param int room id
     * @return array of automation objects or empty array
     */
    public function get_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($room_id)) return array();

        $this->db->from('automation')->where('room_id', $room_id)
            ->order_by('id', 'desc');

        $query = $this->db->get();

        return $query->result();
    }

    /**
     * Edit current automation
     *
     * @param int automation id
     * @param str name
     * @param array of preset values
     * @param array of sensor values
     * @return bool query result
     */
    public function edit($id = FALSE, $name = '', $presets = array(), $sensors = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id)) return FALSE;

        $this->id = $id;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();

        if ($name !== ''):
            $this->db->where('id', $id);
            $this->db->update('automation', array('name' => $name));
        endif;

        // Loops through the data to add the automation id
        foreach($sensors as $key => $value)
        {
            $this->db->where('automation_id', $this->id);
            $this->db->update('automation_sensor', $value);
        }

        foreach($presets as $key => $value)
        {
            $this->db->where('automation_id', $this->id);
            $this->db->update('automation_preset', $value);
        }

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Get state of automation
     *
     * @param int automation id
     * @return associative array of automation data
     * | automation_id | preset_id | automation_name | preset_name
     * | sensor_id | sensor_name | sensor_value | more_than | max_value | type
     */
    public function get_state($id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id)) return array();

        $this->db->select('ap.*', FALSE);
        $this->db->select('a.name AS automation_name', FALSE);
        $this->db->select('pr.name AS preset_name', FALSE);
        $this->db->select('ats.*', FALSE);
        $this->db->select('ss.max_value, ss.type, ss.name AS sensor_name', FALSE);
        $this->db->from('automation_preset AS ap');
        $this->db->join('automation AS a', 'ap.automation_id = a.id');
        $this->db->join('preset AS pr', 'ap.preset_id = pr.id');
        $this->db->join('automation_sensor AS ats', 'ats.automation_id = a.id');
        $this->db->join('sensor AS ss', 'ss.id = ats.sensor_id');
        $this->db->where('a.id', $id);

        $query = $this->db->get();

        return $query->row();
    }

    /**
     * Updates the status of automation by id
     *
     * @param int automation id
     * @param int enabled
     * @return bool status of operation
     */
    public function update_status($id = FALSE, $enabled = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id) OR $enabled === '') return FALSE;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();

        $this->db->where('id', $id);

        $this->db->update('automation', array('enabled' => $enabled));

        $this->db->trans_complete();

        return $this->db->trans_status();
    }
}

/* End of file automation.php */
/* Location: ./app/SmartHome/models/automation.php */
