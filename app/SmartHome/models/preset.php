<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Preset Model
 *
 * Preset Model handles every transaction
 * between the database and the controller.
 *
 * @package     SmartHome
 * @category    Model
 * @author      Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Preset extends CI_Model {

    public $id = '';
    public $name = '';
    public $room_id = '';

    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());
    }

    /**
     * Get a preset by id
     *
     * @param int preset id
     * @return preset object
     */
    public function get($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($preset_id)) return NULL;

        $this->db->from('preset')->where('id', $preset_id);

        $query = $this->db->get();

        if ($query->num_rows() === 1)
            return $query->row();

        else
            return NULL;
    }

    /**
     * Get all presets with the room_id
     *
     * @param int room_id
     * @return array of preset objects or empty array
     */
    public function get_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($room_id)) return array();

        $this->db->from('preset')->where('room_id', $room_id)
            ->order_by('id', 'desc');

        $query = $this->db->get();

        return $query->result();
    }

    /**
     * Get actuator state from preset
     *
     * @param int preset id
     * @return associative array of actuator_id and values
     */
    public function get_state($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $state = array();

        if (empty($preset_id)) return $state;

        $this->db->from('preset_actuator')->where('preset_id', $preset_id);
        $query = $this->db->get();


        foreach($query->result() as $row)
        {
            $state[$row->actuator_id]['value'] = $row->actuator_value;
            $state[$row->actuator_id]['enabled'] = $row->enabled;
        }

        return $state;
    }

    /**
     * Add new preset to database.
     * 3rd parameter is an associative array.
     * e.g array('actuator_id' => 3, 'actuator_value' => 2)
     *
     * @param str name of preset
     * @param int room id
     * @param array of an associative array with actuator_id and value
     *          preset_id will be added after querying
     * @return TRUE if successful, FALSE if fail
     */
    public function add($name = '', $room_id = FALSE, $preset_data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($name) OR empty($room_id)) return FALSE;

        $this->name = $name;
        $this->room_id = $room_id;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();
        $this->db->insert('preset', $this);

        // Get id of last INSERT query
        $this->id = $this->db->insert_id();

        // Loops through the preset_data to add the preset_id
        // Result of the data should look like this
        // array(
        //  array(preset_id, actuator_id, actuator_value, enabled),
        //  array(preset_id, actuator_id, actuator_value, enabled),
        //  ...
        // )
        foreach($preset_data as $key => $actuator)
        {
            $preset_data[$key]['preset_id'] = $this->id;

            // checking for all actuators
            if ( ! isset($actuator['enabled'])):
                $preset_data[$key]['enabled'] = FALSE;
                $preset_data[$key]['actuator_value'] = 0;
            endif;

            // checking for light value
            if ($actuator['type'] == LIGHT):
                if (isset($actuator['on'])):
                    // `preset_actuator` table has no column `on`
                    unset($preset_data[$key]['on']);

                else:
                    $preset_data[$key]['actuator_value'] = 0;

                endif;
            endif;

            // `preset_actuator` table has no column `type`
            unset($preset_data[$key]['type']);
        }

        log_msg(__CLASS__, serialize($preset_data), array());

        $this->db->insert_batch('preset_actuator', $preset_data);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Edit existing preset
     *
     * @param int preset id
     * @param str name of preset
     * @param array of an associate array of actuator values
     * @return TRUE if successful, FALSE if fail
     */
    public function edit($preset_id = FALSE, $name = '', $preset_data = array())
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($preset_id)) return FALSE;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();
        if ( ! empty($name)):
            $this->db->where('id', $preset_id);
            $this->db->set('name', $name);
            $this->db->update('preset');
        endif;

        foreach($preset_data as $key => $actuator):

            // checking for all actuators
            if ( ! isset($actuator['enabled'])):
                $actuator['enabled'] = FALSE;
                $actuator['actuator_value'] = 0;
            endif;

            // checking for light values
            if ($actuator['type'] == LIGHT):
                if (isset($actuator['on'])):
                    // `preset_actuator` table has no column `on`
                    unset($actuator['on']);

                else:
                    $actuator['actuator_value'] = 0;

                endif;
            endif;

            // `preset_actuator` table has no column `type`
            unset($actuator['type']);

            $this->db->where('preset_id', $preset_id);
            $this->db->where('actuator_id', $actuator['actuator_id']);
            $this->db->update('preset_actuator', $actuator);

        endforeach;

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Delete preset from database by id
     *
     * @param int preset id
     * @return bool status of deletion
     */
    public function delete($preset_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($preset_id)) return FALSE;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();

        $this->db->where('preset_id', $preset_id);
        $this->db->delete('preset_actuator');

        $this->db->where('id', $preset_id);
        $this->db->delete('preset');

        $this->db->trans_complete();

        return $this->db->trans_status();
    }
}

/* End of file preset.php */
/* Location: ./app/SmartHome/models/preset.php */

