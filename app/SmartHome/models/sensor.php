<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Sensor Model
 *
 * Sensor Model handles every transaction
 * between the database and the controller.
 *
 * @package     SmartHome
 * @category    Model
 * @author      Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Sensor extends CI_Model {

    public $id = '';
    public $name = '';
    public $type = '';
    public $room_id = '';
  
	public function __construct()
	{
		parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());
	}

    /**
     * Get all sensors in room
     *
     * @param int room id
     * @return array of sensor objects
     */
    public function get_by_room($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($room_id)) return array();
        $this->db->from('sensor');
        $this->db->where('room_id', $room_id);

        $query = $this->db->get();

        return $query->result();
    }
}

/* End of file sensor.php */
/* Location: ./app/SmartHome/models/sensor.php */

