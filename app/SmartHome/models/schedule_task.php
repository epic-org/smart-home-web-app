<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * SmartHome Schedule Task Model
 *
 * Schedule Task Model handles every transaction
 * between the database and the controller.
 *
 * @package     SmartHome
 * @category    Model
 * @author      Epic.org
 * @link        https://bitbucket.org/epic-org/smart-home-web-app
 */
class Schedule_task extends CI_Model {

    public $id = '';
    public $name = '';
    public $date = '';
    public $time = '';
    public $type = '';
    public $enabled = '';
    public $room_id = '';

	public function __construct()
	{
		parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());
	}

    /**
     * Get all schedules by room
     *
     * @param int room id
     * @return array of schedules object
     */
	public function get_by_room($room_id = FALSE)
	{
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($room_id)) return array();

        $this->db->from('schedule_task')->where('room_id', $room_id)
            ->order_by('id', 'desc');

        $query = $this->db->get();

        return $query->result();
	}

    /**
     * Get state of schedule by id
     *
     * @param int schedule id
     * @return associative array of schedule data
     */
    public function get_state($schedule_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($schedule_id)) return array();

        $this->db->from('schedule_task AS st')
            ->join('scheduled_preset AS sp', 'st.id = sp.schedule_task_id')
            ->where('id', $schedule_id);

        $query = $this->db->get();

        return $query->row();
    }

    /**
     * Add new schedule task to db
     *
     * @param array of schedule data [name] [day] [time] [date]
     * @param int preset_id to trigger
     * @param int room id
     * @return int new schedule id
     */
    public function add($schedule = array(), $preset_id = FALSE, $room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($room_id) OR empty($preset_id)) return FALSE;

        $schedule['enabled'] = TRUE;
        $schedule['room_id'] = $room_id;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();
        $this->db->insert('schedule_task', $schedule);

        $this->id = $this->db->insert_id();

        $this->db->insert('scheduled_preset', array(
            'schedule_task_id' => $this->id,
            'preset_id' => $preset_id ));

        $this->db->trans_complete();

        if ($this->db->trans_status())
            return $this->id;
    }

    /**
     * Edit schedule task by id
     *
     * @param int schedule id
     * @param array of schedule data
     * @param int preset id
     * @return bool status of operation
     */
    public function edit($schedule_id = FALSE, $schedule = array(), $preset_id)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($schedule_id) OR empty($preset_id)) return array();

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();
        $this->db->where('id', $schedule_id);
        $this->db->update('schedule_task', $schedule);

        $this->db->where('schedule_task_id' , $schedule_id);
        $this->db->update('scheduled_preset', array('preset_id' => $preset_id));
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Delete schedule task by id
     *
     * @param int schedule id
     * @return bool status of deletion
     */
    public function delete($schedule_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($schedule_id)) return FALSE;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();

        $this->db->where('schedule_task_id', $schedule_id);
        $this->db->delete('scheduled_preset');

        $this->db->where('id', $schedule_id);
        $this->db->delete('schedule_task');

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Updates the status of schedule by id
     *
     * @param int schedule task id
     * @param int enabled
     * @return bool status of operation
     */
    public function update_status($id = FALSE, $enabled = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id) OR $enabled === '') return FALSE;

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();

        $this->db->where('id', $id);

        $this->db->update('schedule_task', array('enabled' => $enabled));

        $this->db->trans_complete();

        return $this->db->trans_status();
    }
}

/* End of file schedule_task.php */
/* Location: ./app/SmartHome/models/schedule_task.php */

