<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_account extends CI_Model {

    public $account_type = '';
    public $password = '';
    public $pin = '';
    public $email = '';

    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());
    }

    /**
     * Get master user's email only if it matches
     *
     * @param str master user email
     *
     * @return str master user email
     */
    public function get_master_email($email = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->db->from('user_account')
            ->select('email')
            ->where('email', $email)
            ->where('account_type', MASTER);

        $query = $this->db->get();

        if ($query->num_rows() === 1)
            return $query->row()->email;

        else
            return FALSE;
    }

    /**
     * Get user's existence by account type and password combination
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str password SHA1 Hashed
     *
     * @return bool TRUE if successful
     */
    public function get_user_by_password($account_type = FALSE, $password = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($account_type) OR empty($password)):
            return FALSE;
        endif;

        $this->db->from('user_account')
            ->where('account_type', $account_type)
            ->where('password', $password)
            ->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() === 1)
            return TRUE;

        else
            return FALSE;
    }

    /**
     * Get user type by pin
     *
     * @param str pin SHA1 Hashed
     *
     * @return int account_type , FALSE if fail.
     */
    public function get_user_by_pin($pin = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($pin)) return FALSE;

        $this->db->from('user_account')
            ->where('pin', $pin)
            ->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() > 0):
            $user = $query->row();

            return $user->account_type;
        else:
            return FALSE;
        endif;
    }

    /**
     * Update user's pin by account type
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str pin SHA1 Hashed
     * @return bool status of operation
     */
    public function update_pin($account_type = FALSE, $pin = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($account_type) OR empty($pin)) return FALSE;

        $this->db->where('account_type', $account_type);

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();
        $this->db->update('user_account', array('pin' => $pin));
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Update user's password by account type
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str pin SHA1 Hashed
     * @return bool status of operation
     */
    public function update_password($account_type = FALSE, $password = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($account_type) OR empty($password)) return FALSE;

        $this->db->where('account_type', $account_type);

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();
        $this->db->update('user_account', array('password' => $password));
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Update user's email by account type
     *
     * @param int account_type (MASTER, NORMAL)
     * @param str email
     * @return bool status of operation
     */
    public function update_email($account_type = FALSE, $email = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($account_type) OR empty($email)) return FALSE;

        $this->db->where('account_type', $account_type);

        $this->db->trans_strict(FALSE);
        $this->db->trans_start();
        $this->db->update('user_account', array('email' => $email));
        $this->db->trans_complete();

        return $this->db->trans_status();
    }
}

/* End of file user_account.php */
/* Location ./app/SmartHome/models/user_account.php */
