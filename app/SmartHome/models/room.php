<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Room extends CI_Model {

    public $id = '';
    public $name = '';

    public function __construct()
    {
        parent::__construct();
        log_msg(__CLASS__, __FUNCTION__, func_get_args());
    }

    /**
     * Get all rooms from table `room`
     *
     * @return array of room objects, FALSE if no result
     */
    public function get_all()
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        $this->db->from('room');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->result();

        else
            return FALSE;
    }

    /**
     * Adds new room to table `room`
     *
     * @param int room id
     * @param str name
     *
     * @return bool status of operation
     */
    public function add($id = FALSE, $name = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id) OR empty($name)) return FALSE;

        $this->id = $id;
        $this->name = $name;
        $this->db->insert('room', $this);

        return TRUE;
    }

    /**
     * Get room by id
     *
     * @return room object, FALSE if no result
     */
    public function get($id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id)) return NULL;

        $this->db->from('room')
            ->where('id', $id)
            ->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1)
            return $query->row();

        else
            return NULL;
    }

    /**
     * Update room name
     *
     * @param int room_id
     * @param str new room name
     * @return TRUE if successful, FALSE otherwise
     */
    public function edit($id = FALSE, $name = '')
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($id) OR empty($name)) return FALSE;

        $this->db->set('name', $name);
        $this->db->where('id', $id);
        $this->db->update('room');

        return TRUE;
    }

    /**
     * Get room name
     *
     * @param int room id
     * @return associative array
     */
    public function get_name($room_id = FALSE)
    {
        log_msg(__CLASS__, __FUNCTION__, func_get_args());

        if (empty($room_id)) return FALSE;

        $this->db->select('name')
            ->from('room')
            ->where('id', $room_id);
        $query = $this->db->get();

        return $query->row_array();
    }
}

/* End of file room.php */
/* Location: ./app/SmartHome/models/room.php */
