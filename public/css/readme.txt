-- This folder contains all custom CSS & scss. --

DO NOT EDIT "custom.css" MANUALLY!
ONLY EDIT "custom.scss".
** You must have Sass installed on your machine.
[Windows] When editing "custom.scss", run "watch custom.bat" to watch for any changes made to "custom.scss". This will compile the scss to CSS when any changes are made.



** Please refer to 
"http://sass-lang.com/documentation/file.SASS_REFERENCE.html#using_sass" **