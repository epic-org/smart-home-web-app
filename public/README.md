# Structure of directory listing

- assets
- css
- img
- js

All 3rd party libraries should be places under `assets`
E.g `assets/jquery-mobile/` and all jquery-mobile dependencies should be in that folder (img, css, js)

### NOTE: Large images shouldn't be committed. It should be uploaded seperately to the server.

- Any files that is our own (css img, js) should be placed in the respective folders.
- Images are not committed into the repo to save space.
- Images are storred in the web server locally.
- Images should be compressed accordingly
- **Locally, images may exist but never committed**



